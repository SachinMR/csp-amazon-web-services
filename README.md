
# Setup of AWS credentials and services

## Prerequisites
- An AWS account.
  - In case the user does not have AWS account follow steps in this [link](https://docs.aws.amazon.com/accounts/latest/reference/manage-acct-creating.html) to create AWS account.
- IAM user created for AWS account
  - To create IAM user for a particular user follow steps in this [link](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html)
  - To create AWS Access Key and AWS Secret Key follow the steps in this [link](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html)
- The following services should be enabled on AWS account.
  - AWS IoT Core
  - AWS Greengrass
  - AWS Cloudwatch
  - AWS Lambda
  - AWS ECR
  - AWS EC2
  - AWS S3 storage

# Follow below sequence in order to setup this repo

## Clone the repository
- Clone the repository on target device using below command.
```sh
git clone git@gitlab.com:Linaro/blueprints/software-defined-camera/reference-use-cases/traffic-monitoring/csp-integration/csp-amazon-web-services.git
```
## Configure Greengrass on target device.
- Follow the steps in document docs/ARM_Smart_Camera_CSP_Configure_Greengrass_on_Device.pdf for configuring greengrass on device.

## Building the Cloud container
- Follow the document Cloud_container/README.md for building and deploying the cloud container.

## Orchestrating the Containers through AWS IoT Greengrass service.
- Follow the document docs/ARM_Smart_Camera_CSP_Greengrass_Orchestration.pdf for Orchestration of all containers through AWS Greengrass.

## Setup the Cloudwatch dashboard.
- Follow the document Cloudwatch/docs/ARM_Smart_Camera_CSP_Setting_up_Cloudwatch_Dashboard.pdf for setting the cloudwatch dashboard.
- Note that before following this document we need to run the inference pipeline atleast once to push the metrics to cloudwatch servive.

## Deploying all the containers using kubernetes.
- If the user wants to orchestrate the containers using kubernetes follow the document at kubernetes/docs/ARM_Smart_camera_CSP_Deployment_on_kubernetes.pdf.

## Testing the deployed pipeline with AWS IoT Device Tester.
- For testing the pipeline with AWS IoT Device Tester, refer the document at 
