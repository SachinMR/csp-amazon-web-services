# AWS IoT Device Tester Java Client SDK

This SDK contains functions that can be used to interact with IDT.

## Overview

The files provided look like this:
```
idt/
├── IDTClient.java
├── exception
    └── ...
├── ...
└── model
    └── ...
```

Where:
- `IDTClient.java` contains the `IDTClient` interface and `create` function - the access point to communicate with IDT.
- `exception` contains some IDT specific exceptions that can be raised while using the client.
- `model` contains type definitions for client requests/responses.

## Prerequisites

The client SDK needs Java:
* Java (>=8)

and needs the following third party packages:
* Jackson-databind (2.12.x) - https://github.com/FasterXML/jackson-databind
* Jackson-annotations (2.12.x) - https://github.com/FasterXML/jackson-annotations/tree/2.12
* Jackson-core (2.12.x) - https://github.com/FasterXML/jackson-core
* Log4j-api (2.x) - https://logging.apache.org/log4j/2.x/log4j-api/
* Maven-com-google-code-findbugs_jsr305 (3.x) - https://mvnrepository.com/artifact/com.google.code.findbugs/jsr305
* Maven-org-jetbrains_annotations (13.x) - http://www.jetbrains.org/display/IJOS/Home;jsessionid=1881AA3B9F1A3A8D98C0EDD69082DC85
* Maven-com-squareup-retrofit2_converter-jackson (2.x) - https://mvnrepository.com/artifact/com.squareup.retrofit2/converter-jackson
* Maven-com-squareup-retrofit2_retrofit (2.x) - https://mvnrepository.com/artifact/com.squareup.retrofit2/retrofit
* Maven-com-squareup-okhttp3_okhttp (4.x) - https://mvnrepository.com/artifact/com.squareup.okhttp3/okhttp
* Maven-com-squareup-okio_okio (2.x) - https://mvnrepository.com/artifact/com.squareup.okio/okio
* Maven-org-jetbrains-kotlin_kotlin-stdlib (1.x) - https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib
* Maven-org-jetbrains-kotlin_kotlin-stdlib-common (1.x) - https://mvnrepository.com/artifact/org.jetbrains.kotlin/kotlin-stdlib-common

## Using the SDK in your project

The `idt` folder should be copied into your test suite code. An example is shown in the Java sample suite.
Note that you can copy it to any directory so long as it is importable.

## Example usage

To use the SDK, create a `Client` by calling `create`. Then, call functions on the client using request models as parameters. Response models will be returned.

If the `idt` folder is in the root of your Java project, then you can do (error handling omitted):

```
import idt.IDTClient;
import idt.ServiceConfiguration;
import idt.model.*;

public static void main(String[] args) {
    ServiceConfiguration defaultConfig = ServiceConfiguration.builder().build();
    
    IDTClient client = IDTClient.create(ServiceConfiguration.builder()
            .from(defaultConfig)
            .readTimeoutInMinutes(1)
            .build());
            
    TestResult testResult = TestResult.builder()
                .passed(true)
                .build();
                
    SendResultResponse sendResultResponse = client.sendResult(SendResultRequest.builder()
            .testResult(testResult)
            .build());
}
```

## License

The client SDK is licensed under the Apache-2.0  license.