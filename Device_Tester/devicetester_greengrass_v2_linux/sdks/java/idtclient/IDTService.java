// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt;

import com.amazonaws.iot.idt.model.CopyToDeviceRequest;
import com.amazonaws.iot.idt.model.CopyToDeviceResponse;
import com.amazonaws.iot.idt.model.ExecuteOnDeviceRequest;
import com.amazonaws.iot.idt.model.ExecuteOnDeviceResponse;
import com.amazonaws.iot.idt.model.ExecuteOnHostRequest;
import com.amazonaws.iot.idt.model.ExecuteOnHostResponse;
import com.amazonaws.iot.idt.model.GetContextValueRequest;
import com.amazonaws.iot.idt.model.GetContextValueResponse;
import com.amazonaws.iot.idt.model.PollForNotificationsRequest;
import com.amazonaws.iot.idt.model.PollForNotificationsResponse;
import com.amazonaws.iot.idt.model.ReadFromDeviceRequest;
import com.amazonaws.iot.idt.model.ReadFromDeviceResponse;
import com.amazonaws.iot.idt.model.SendResultRequest;
import com.amazonaws.iot.idt.model.SendResultResponse;
import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.jackson.JacksonConverterFactory;
import retrofit2.http.Body;
import retrofit2.http.POST;

import java.util.concurrent.TimeUnit;

interface IDTService {
    @POST("executeOnHost/v1")
    Call<ExecuteOnHostResponse> executeOnHost(@Body ExecuteOnHostRequest request);

    @POST("copyToDevice/v1")
    Call<CopyToDeviceResponse> copyToDevice(@Body CopyToDeviceRequest request);

    @POST("executeOnDevice/v1")
    Call<ExecuteOnDeviceResponse> executeOnDevice(@Body ExecuteOnDeviceRequest request);

    @POST("sendResult/v1")
    Call<SendResultResponse> sendResult(@Body SendResultRequest request);

    @POST("pollForNotifications/v1")
    Call<PollForNotificationsResponse> pollForNotifications(@Body PollForNotificationsRequest request);

    @POST("readFromDevice/v1")
    Call<ReadFromDeviceResponse> readFromDevice(@Body ReadFromDeviceRequest request);

    @POST("getContextValue/v1")
    Call<GetContextValueResponse> getContextValue(@Body GetContextValueRequest request);

    static IDTService create(ObjectMapper mapper, IDTServiceConfiguration configuration) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(String.format("http://localhost:%d", configuration.port()))
                .addConverterFactory(JacksonConverterFactory.create(mapper))
                .client(new OkHttpClient.Builder()
                        .addInterceptor(new IDTCredentialsInterceptor(mapper, configuration))
                        .readTimeout(configuration.readTimeoutInMinutes(), TimeUnit.MINUTES)
                        .build())
                .build();
        return retrofit.create(IDTService.class);
    }
}
