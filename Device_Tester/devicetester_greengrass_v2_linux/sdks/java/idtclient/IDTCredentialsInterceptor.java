// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt;

import com.amazonaws.iot.idt.exception.IDTServiceException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.JsonNodeType;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okio.BufferedSink;
import okio.Okio;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

class IDTCredentialsInterceptor implements Interceptor {
    private static final Logger LOGGER = LogManager.getLogger(IDTCredentialsInterceptor.class);
    private static final MediaType JSON = MediaType.get("application/json");

    private final IDTServiceConfiguration configuration;
    private final ObjectMapper mapper;

    IDTCredentialsInterceptor(final ObjectMapper mapper, final IDTServiceConfiguration configuration) {
        this.configuration = configuration;
        this.mapper = mapper;
    }

    @NotNull
    @Override
    public Response intercept(@NotNull Chain chain) throws IOException {
        Request request = chain.request();
        RequestBody body = request.body();

        if (Objects.isNull(body)) {
            return chain.proceed(request);
        }

        LOGGER.debug("Injecting credentials for {} in request: {}", configuration.uniqueId(), request.url());
        RequestBody newBody = RequestBody.create(authorize(payload(body)), JSON);
        Request newRequest = request.newBuilder()
                .method(request.method(), newBody)
                .build();
        LOGGER.debug("Updated request: {}", newRequest);
        Response response = chain.proceed(newRequest);
        return enrich(response, response.body());
    }

    private Response enrich(Response existing, ResponseBody body) throws IOException {
        if (Objects.isNull(body)) {
            return existing;
        }
        JsonNode result = mapper.readTree(body.byteStream());
        LOGGER.debug("Original response status: {} - {}", existing.code(), result);
        if (result.has("data") && result.get("data").getNodeType() != JsonNodeType.NULL) {
            return existing.newBuilder()
                    .body(ResponseBody.create(mapper.writeValueAsBytes(result.get("data")), JSON))
                    .build();
        } else {
            throw new IDTServiceException(
                    result.get("errorMessage").asText(),
                    existing.headers().toMultimap(),
                    existing.code());
        }
    }

    private byte[] payload(RequestBody body) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        BufferedSink sink = Okio.buffer(Okio.sink(baos));
        body.writeTo(sink);
        sink.flush();
        return baos.toByteArray();
    }

    private String authorize(byte[] payload) throws IOException {
        Map<String, Object> json = mapper.readValue(payload, HashMap.class);
        json.put("secret", configuration.secret());
        json.put("uniqueId", configuration.uniqueId());
        LOGGER.debug("Hydrated payload: {}", json);
        return mapper.writeValueAsString(json);
    }
}
