// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

/**
 * The result of a test case execution.
 *
 * In order to send a well formed request, exactly one of the following must be true:
 *    - failure is not empty
 *    - error is not empty
 *    - skipped is not empty
 *    - passed is true
 */
@Value.Immutable
@IDTModel
@JsonSerialize(as = TestResult.class)
interface IDTTestResult {

    /**
     * Indication that the test was successfully executed.
     */
    @Value.Default
    default boolean passed() {
        return false;
    }

    /**
     * Execution time of the test case in seconds.
     * If set to empty, test case execution time will be omitted in the generated report.
     * If set to 0, test case execution time will show in the report
     * and be set to the elapsed time since the test case executable was launched.
     * Otherwise, test case execution time will show in the report as this value.
     *
     * Note: regardless of the above, test group execution time will show in the generated report
     * as the elapsed time all test case executables in the group took to run.
     */
    @Nullable
    Integer duration();

    /**
     * Info about the test case error. Leave empty if the test didn't error.
     */
    @Nullable
    ErrorMessage error();

    /**
     * Info about the test case failure. Leave empty if the test didn't fail.
     */
    @Nullable
    FailureMessage failure();

    /**
     * Info describing why the test case was skipped. Leave empty if the test wasn't skipped.
     */
    @Nullable
    SkippedMessage skipped();

    /**
     * The name of the test case that ran.
     * If left blank, will default to test case ID.
     */
    @Nullable
    String testCaseName();

    /**
     * The category name of the test case that ran.
     * Will map to JUnit's "classname" attribute in the final test report.
     * If left blank, will default to the concatenation of the suite ID and group ID of the test case.
     */
    @Nullable
    String testCategoryName();
}
