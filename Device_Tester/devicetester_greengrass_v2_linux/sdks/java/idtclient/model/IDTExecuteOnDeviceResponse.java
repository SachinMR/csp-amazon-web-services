// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@IDTModel
@JsonDeserialize(builder = ExecuteOnDeviceResponse.Builder.class)
interface IDTExecuteOnDeviceResponse {
    // The response to an execute on device request.

    /**
     * The standard output of the command.
     * May be an empty slice if there was no output.
     */
    byte[] stdout();

    /**
     * The standard error of the command.
     * May be an empty slice if there was no error output.
     */
    byte[] stderr();

    /**
     * The exit code of the command.
     */
    int exitCode();

    /**
     * Whether the command timed out or not.
     */
    boolean timedOut();
}
