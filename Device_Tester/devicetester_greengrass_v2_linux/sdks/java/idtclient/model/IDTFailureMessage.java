// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

@Value.Immutable
@IDTModel
@JsonSerialize(as = FailureMessage.class)
interface IDTFailureMessage {
    // Info about a test failure. This should be used when the test logic runs, but assertions/checks fail.

    /**
     * The type of the failure.
     */
    String type();

    /**
     * More details about the failure.
     */
    String message();
}
