// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

/**
 * The response to a read from device request.
 */
@IDTModel
@Value.Immutable
@JsonDeserialize(builder = ReadFromDeviceResponse.Builder.class)
interface IDTReadFromDeviceResponse {

    /**
     * The bytes that were read. May be empty if no data was read.
     */
    byte[] bytes();

    /**
     * Whether an end of file indicator was encountered when reading from the device.
     */
    boolean eof();
}
