// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.immutables.value.Value;

import javax.annotation.Nullable;

@Value.Immutable
@IDTModel
@JsonSerialize(as = ExecuteOnHostRequest.class)
interface IDTExecuteOnHostRequest {
    // An execute on host request.

    /**
     * The command to execute.
     */
    ExecuteOnHostCommand command();

    /**
     * The file path where IDT will store the process standard output.
     * If relative, will be considered relative to the test case result directory,
     * which is automatically deleted afterwards.
     * If omitted, a file name will be automatically generated and placed in IDT execution's result directory.
     * If the target file already exists, new contents will be appended to it.
     */
    @Nullable
    String stdOutFile();

    /**
     * The file path where IDT will store the process standard error output.
     * If relative, will be considered relative to the test case result directory,
     * which is automatically deleted afterwards.
     * If omitted, a file name will be automatically generated and placed in IDT execution's result directory.
     * If the target file already exists, new contents will be appended to it.
     */
    @Nullable
    String stdErrFile();
}
