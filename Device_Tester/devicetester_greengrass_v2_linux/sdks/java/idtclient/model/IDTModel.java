// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import org.immutables.value.Value;

@Value.Style(
        jdkOnly = true,
        get = { "get*", "is*" },
        typeAbstract = "IDT*",
        typeImmutable = "*",
        allMandatoryParameters = true,
        visibility = Value.Style.ImplementationVisibility.PUBLIC)
public @interface IDTModel {
}
