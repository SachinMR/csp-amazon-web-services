// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package com.amazonaws.iot.idt.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.immutables.value.Value;

@Value.Immutable
@IDTModel
@JsonDeserialize(builder = PollForNotificationsResponse.Builder.class)
interface IDTPollForNotificationsResponse {
    // The response to a poll for notifications request.

    /**
     * This field is set to true when IDT requests test cases to stop running, cleanup, and exit.
     *
     * The value will be true for the duration of the cancellation request
     *   - i.e. repeated polling will continue to retrieve true even after the first call.
     * Test cases should take care to only run any necessary cleanup actions once.
     */
    boolean cancellationRequested();
}
