# AWS IoT Device Tester Python Client SDK

This SDK contains functions that can be used to interact with IDT.

## Overview

The files provided look like this:
```
idt_client/
├── client.py
├── exceptions.py
├── ...
└── models
    └── ...
```

Where:
- `client.py` contains the `Client` class - the access point to communicate with IDT.
- `exceptions.py` contains some IDT specific exceptions that can be raised while using the client.
- `models` contains type definitions for client requests/responses.

## Prerequisites

The client SDK needs Python:
* Python (>=3.7.0 is recommended, >=3.6.0 is required) - https://python.org/
  * Must be available as `python3` from the PATH variable.

and needs the following third party packages:
* urllib3 - https://pypi.org/project/urllib3/
  * Must be importable when using the `python3` command.

## Using the SDK in your project

The `idt_client` folder should be copied into your test suite code. An example is shown in the Python sample suite.
Note that you can copy it to any directory so long as it is importable.

## Example usage

To use the SDK, create a `Client`. Then, call functions on the client using request models as parameters. Response models will be returned.

If the `idt_client` folder is in the same folder as your Python file, then you can do (error handling omitted):

```
from idt_client import *

def main():
    client = Client()

    request = SendResultRequest(TestResult(passed=True))

    client.send_result(sr_req)

if __name__ == "__main__":
    main()
```

## License

The client SDK is licensed under the Apache-2.0  license.
