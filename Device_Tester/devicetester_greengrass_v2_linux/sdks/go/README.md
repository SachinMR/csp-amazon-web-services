# AWS IoT Device Tester Go Client SDK

This SDK contains functions that can be used to interact with IDT.

## Overview

The files provided look like this:
```
idtclient/
├── client.go
├── errors.go
├── ...
└── models
    └── ...
```

Where:
- `client.go` contains the `Client` interface and `NewClient` function - the access point to communicate with IDT.
- `errors.go` contains some IDT specific errors that can be returned when using the client.
- `models` contains type definitions for client requests/responses.

## Prerequisites

The client SDK needs Go:
* Go (>=1.12) - https://golang.org/

and needs no third party packages.

## Using the SDK in your project

The `idclient` folder should be copied into your test suite code. An example is shown in the Go sample suite.
Note that you can copy it to any directory so long as it is importable.

## Example usage

To use the SDK, create a `Client` by calling `NewClient`. Then, call functions on the client using request models as parameters. Response models will be returned.

If the `idtclient` folder is in the root of your Go project, then you can do (error handling omitted):

```
import (
    "idtclient"
    "idtclient/models"
)

func main() {
    idtClient, _ := idtclient.NewClient()

    request := models.SendResultRequest{TestResult: models.TestResult{Passed: true}}

    idtClient.SendResult(request)
}
```

## License

The client SDK is licensed under the Apache-2.0  license.
