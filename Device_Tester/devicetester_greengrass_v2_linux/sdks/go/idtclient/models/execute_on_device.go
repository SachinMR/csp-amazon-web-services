// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package models

import "time"

// An execute on device request.
type ExecuteOnDeviceRequest struct {
	// The name of the device to execute the command on.
	// Leave empty to use the device under test. Otherwise, use the name specified in the test.json for resource devices.
	DeviceName string `json:"deviceName"`

	// The command to execute.
	Command ExecuteOnDeviceCommand `json:"command"`

	// Standard input to provide to the command.
	StdIn []byte `json:"stdin"`
}

// The details of the command to run on the device.
type ExecuteOnDeviceCommand struct {
	// The command line to execute.
	CommandLine string `json:"commandLine"`

	// Environment variables to be set before the command runs, where keys are environment variable names and value are the corresponding values.
	// These are set only for the environment the command runs in, and changes to the environment do not persist after the command finishes.
	//
	// A note if the device under test is communicating with IDT over SSH:
	//	 It is common for SSH servers to only allow certain environment variable names to be set, e.g. most sshd daemons on linux only allow names that start with 'LC_'.
	//	 In order to use this parameter the SSH server on the DUT must be able to set environment variables and must accept the provided environment variable names, or the request will fail.
	//   If this is an issue, you can instead prepend environment variables to the start of the command to set any variable using appropriate shell syntax.
	EnvVars map[string]string `json:"envVars"`

	// The timeout to wait for the command to complete before forcefully killing it. Leave as 0 for no timeout.
	// Any standard output/standard error will be returned, up until the command was killed. If the command times out, the exit code returned will not be meaningful.
	Timeout time.Duration `json:"timeout"`
}

// The response to an execute on device request.
type ExecuteOnDeviceResponse struct {
	// The standard output of the command. May be an empty slice if there was no output.
	StdOut []byte `json:"stdout"`

	// The standard error of the command. May be an empty slice if there was no error output.
	StdErr []byte `json:"stderr"`

	// The exit code of the command.
	ExitCode int `json:"exitCode"`

	// Whether the command timed out or not.
	TimedOut bool `json:"timedOut"`
}
