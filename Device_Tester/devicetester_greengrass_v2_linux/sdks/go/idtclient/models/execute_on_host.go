// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package models

import "time"

// An execute on host request.
type ExecuteOnHostRequest struct {
	// The command to execute.
	Command ExecuteOnHostCommand `json:"command"`

	// The file path where IDT will store the process standard output.
	// If relative, will be considered relative to the test case result directory, which is automatically deleted afterwards.
	// If omitted, a file name will be automatically generated and placed in IDT execution's result directory.
	// If the target file already exists, new contents will be appended to it.
	StdOutFile string `json:"stdOutFile"`

	// The file path where IDT will store the process standard error output.
	// If relative, will be considered relative to the test case result directory, which is automatically deleted afterwards.
	// If omitted, a file name will be automatically generated and placed in IDT execution's result directory.
	// If the target file already exists, new contents will be appended to it.
	StdErrFile string `json:"stdErrFile"`
}

// The details of the command to run on the host.
// Please note that the working directory of the process will be set to the test case directory (where test.json is located).
type ExecuteOnHostCommand struct {
	// The timeout to wait for the command to complete before forcefully killing it. Leave 0 for no timeout.
	// In case of timeout, any standard output/standard error will still be captured, up until the command was killed,
	// and the returned exit code will not be meaningful.
	Timeout time.Duration `json:"timeout"`

	// The file path of the command itself, without any arguments.
	// If consists of a file name only, IDT will look it up using its PATH environment variable.
	// If consists of a relative path, will be considered relative to the test case directory (where test.json is located).
	// Please note that relative paths are not supported in debug-test-suite mode.
	Cmd string `json:"cmd"`

	// The command-line arguments.
	Args []string `json:"args"`

	// Environment variables to be set in the environment your command will run in, in addition to inheriting from IDT's environment.
	// The environment variables provided here have precedence over the ones in IDT's environment in case of name conflicts.
	EnvVars map[string]string `json:"envVars"`
}

// The response to an execute on host request.
type ExecuteOnHostResponse struct {
	// The process exit code of the command.
	ExitCode int `json:"exitCode"`

	// Whether the command timed out or not.
	TimedOut bool `json:"timedOut"`

	// The absolute file path where IDT stored the process standard output.
	// Useful if you did not provide one as part of the request.
	StdOutFile string `json:"stdOutFile"`

	// The absolute file path where IDT stored the process standard error output.
	// Useful if you did not provide one as part of the request.
	StdErrFile string `json:"stdErrFile"`
}
