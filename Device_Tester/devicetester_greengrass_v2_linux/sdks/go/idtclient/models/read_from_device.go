// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package models

type ReadFromDeviceCommand string

const (
	OpenCommand  ReadFromDeviceCommand = "Open"
	ReadCommand  ReadFromDeviceCommand = "Read"
	CloseCommand ReadFromDeviceCommand = "Close"
)

// A read from device request.
type ReadFromDeviceRequest struct {
	// The name of the device to read from.
	// Leave empty to use the device under test. Otherwise, use the name specified in the test.json for resource devices.
	DeviceName string `json:"deviceName"`

	// The command to send. Must be a ReadFromDeviceCommand.
	// Command info:
	//   - Open causes the underlying connection to be opened. This must be issued before Read.
	//   - Read reads data from the underlying connection and should be called only after Open. Read can be called many times to keep reading data.
	//   - Close causes the underlying connection to be closed. No more Reads should be issued before issuing another Open.
	// IDT will call Close at the end of each test case if the connection is not already closed.
	Command ReadFromDeviceCommand `json:"command"`
}

// The response to a read from device request.
type ReadFromDeviceResponse struct {
	// The bytes that were read. Can be an empty slice if no data was read.
	Bytes []byte `json:"bytes"`

	// Whether an end of file indicator was encountered when reading from the device.
	EOF bool `json:"eof"`
}
