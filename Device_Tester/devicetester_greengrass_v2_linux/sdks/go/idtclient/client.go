// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package idtclient

import (
	"bytes"
	"encoding/json"
	"fmt"
	internalModels "idtclient/internal/models"
	"idtclient/models"
	"io/ioutil"
	"net/http"
	"os"
)

const (
	executeOnHostEndpoint        = "/executeOnHost/v1"
	executeOnDeviceEndpoint      = "/executeOnDevice/v1"
	copyToDeviceEndpoint         = "/copyToDevice/v1"
	readFromDeviceEndpoint       = "/readFromDevice/v1"
	sendResultEndpoint           = "/sendResult/v1"
	pollForNotificationsEndpoint = "/pollForNotifications/v1"
	getContextValueEndpoint      = "/getContextValue/v1"
	portEnvVarName               = "IDT_SERVER_PORT"
	secretEnvVarName             = "IDT_SECRET"
	uniqueIdEnvVarName           = "IDT_UNIQUE_ID"
	secretField                  = "secret"
	uniqueIdField                = "uniqueId"
	getContextStringPath         = "_IDT_GetContextStringPath"
)

// The IDT client.
//
// Use this interface to send requests to IDT and get responses back.
type Client interface {
	// Execute a shell command on the local machine.
	//
	// See the request/response models for more information.
	ExecuteOnHost(request models.ExecuteOnHostRequest) (*models.ExecuteOnHostResponse, error)

	// Copy a file or directory from the machine running IDT to a device.
	//
	// The cat command must be available on the device to copy files. The mkdir and cat commands must be available to copy directories.
	//
	// See the request/response models for more information.
	CopyToDevice(request models.CopyToDeviceRequest) (*models.CopyToDeviceResponse, error)

	// Execute a shell command on a device.
	//
	// See the request/response models for more information.
	ExecuteOnDevice(request models.ExecuteOnDeviceRequest) (*models.ExecuteOnDeviceResponse, error)

	// Read from a device. The response fields should be used only when the Read command was issued without error. They'll be empty if Read was not issued (even if there was no error).
	//
	// This reads from the serial port associated with the device.
	//
	// See the request/response models for more information.
	ReadFromDevice(request models.ReadFromDeviceRequest) (*models.ReadFromDeviceResponse, error)

	// Send the result of the test to IDT.
	//
	// A result should be sent for each test case in the test suite.
	//
	// See the request/response models for more information.
	SendResult(request models.SendResultRequest) (*models.SendResultResponse, error)

	// Receive notifications from IDT about various events that occur.
	//
	// Test cases should continuously call this function at frequent intervals (~200ms) to receive updates about state that IDT wants to communicate with the test cases currently running.
	//
	// See the request/response models for more information.
	PollForNotifications() (*models.PollForNotificationsResponse, error)

	// Get a value from IDT's context.
	//
	// See the request/response models for more information.
	GetContextValue(request models.GetContextValueRequest) (*models.GetContextValueResponse, error)

	// Get a string using IDT's context placeholder replacement.
	//
	// See the request/response models for more information.
	GetContextString(request models.GetContextStringRequest) (*models.GetContextStringResponse, error)
}

// Creates a new IDT client.
func NewClient() (Client, error) {
	port, err := fetchEnv(portEnvVarName, true)
	if err != nil {
		return nil, err
	}

	secret, err := fetchEnv(secretEnvVarName, false)
	if err != nil {
		return nil, err
	}

	uniqueId, err := fetchEnv(uniqueIdEnvVarName, false)
	if err != nil {
		return nil, err
	}

	return &client{
		url:      fmt.Sprintf("http://localhost:%s", port),
		secret:   secret,
		uniqueId: uniqueId,
	}, nil
}

type client struct {
	url      string
	secret   string
	uniqueId string
}

func (c *client) ExecuteOnHost(request models.ExecuteOnHostRequest) (*models.ExecuteOnHostResponse, error) {
	var responseWrapper internalModels.ExecuteOnHostResponseWrapper
	if err := c.sendRequest(executeOnHostEndpoint, request, &responseWrapper); err != nil {
		return nil, err
	}

	if responseWrapper.Data.TimedOut {
		return nil, &ExecuteOnHostCommandTimedOutError{
			Response:     responseWrapper.Data,
			errorMessage: responseWrapper.ErrorMessage,
		}
	}

	if responseWrapper.Data.ExitCode != 0 {
		return nil, &ExecuteOnHostCommandFailedError{
			Response:     responseWrapper.Data,
			errorMessage: responseWrapper.ErrorMessage,
		}
	}

	return responseWrapper.Data, nil
}

func (c *client) CopyToDevice(request models.CopyToDeviceRequest) (*models.CopyToDeviceResponse, error) {
	var responseWrapper internalModels.CopyToDeviceResponseWrapper
	if err := c.sendRequest(copyToDeviceEndpoint, request, &responseWrapper); err != nil {
		return nil, err
	}

	return responseWrapper.Data, nil
}

func (c *client) ExecuteOnDevice(request models.ExecuteOnDeviceRequest) (*models.ExecuteOnDeviceResponse, error) {
	var responseWrapper internalModels.ExecuteOnDeviceResponseWrapper
	if err := c.sendRequest(executeOnDeviceEndpoint, request, &responseWrapper); err != nil {
		return nil, err
	}

	if responseWrapper.Data.TimedOut {
		return nil, &ExecuteOnDeviceCommandTimedOutError{
			Response:     responseWrapper.Data,
			errorMessage: responseWrapper.ErrorMessage,
		}
	}

	if responseWrapper.Data.ExitCode != 0 {
		return nil, &ExecuteOnDeviceCommandFailedError{
			Response:     responseWrapper.Data,
			errorMessage: responseWrapper.ErrorMessage,
		}
	}

	return responseWrapper.Data, nil
}

func (c *client) ReadFromDevice(request models.ReadFromDeviceRequest) (*models.ReadFromDeviceResponse, error) {
	var responseWrapper internalModels.ReadFromDeviceResponseWrapper
	if err := c.sendRequest(readFromDeviceEndpoint, request, &responseWrapper); err != nil {
		return nil, err
	}

	return responseWrapper.Data, nil
}

func (c *client) SendResult(request models.SendResultRequest) (*models.SendResultResponse, error) {
	var responseWrapper internalModels.SendResultResponseWrapper
	if err := c.sendRequest(sendResultEndpoint, request, &responseWrapper); err != nil {
		return nil, err
	}

	return responseWrapper.Data, nil
}

func (c *client) PollForNotifications() (*models.PollForNotificationsResponse, error) {
	var responseWrapper internalModels.PollForNotificationsResponseWrapper
	emptyRequest := map[string]interface{}{}
	if err := c.sendRequest(pollForNotificationsEndpoint, emptyRequest, &responseWrapper); err != nil {
		return nil, err
	}

	return responseWrapper.Data, nil
}

func (c *client) GetContextValue(request models.GetContextValueRequest) (*models.GetContextValueResponse, error) {
	var responseWrapper internalModels.GetContextValueResponseWrapper
	if err := c.sendRequest(getContextValueEndpoint, request, &responseWrapper); err != nil {
		return nil, err
	}

	if len(responseWrapper.Data.UnresolvedJsonPaths) != 0 {
		return nil, &GetContextValueUnresolvedJsonPathsError{
			Response:     responseWrapper.Data,
			errorMessage: responseWrapper.ErrorMessage,
		}
	}

	return responseWrapper.Data, nil
}

func (c *client) GetContextString(request models.GetContextStringRequest) (*models.GetContextStringResponse, error) {
	updatedExtraDefs := map[string]interface{}{getContextStringPath: request.InputString}
	for k, v := range request.ExtraDefs {
		updatedExtraDefs[k] = v
	}

	response, err := c.GetContextValue(models.GetContextValueRequest{
		JsonPath:  getContextStringPath,
		ExtraDefs: updatedExtraDefs,
	})
	if err != nil {
		return nil, err
	}

	var value string
	if err := json.Unmarshal(response.Value, &value); err != nil {
		return nil, err
	}

	return &models.GetContextStringResponse{
		Value:                value,
		EncounteredJsonPaths: response.EncounteredJsonPaths,
		UnresolvedJsonPaths:  response.UnresolvedJsonPaths,
	}, nil
}

func (c *client) sendRequest(endpoint string, request interface{}, result interface{}) error {
	// Marshal request
	buffer := &bytes.Buffer{}
	encoder := json.NewEncoder(buffer)
	encoder.SetEscapeHTML(false)

	err := encoder.Encode(request)
	if err != nil {
		return err
	}

	requestBody, err := ioutil.ReadAll(buffer)
	if err != nil {
		return err
	}

	// Insert secret and unique id into request
	requestBody, err = insertKeyValues(requestBody, map[string]interface{}{
		secretField:   c.secret,
		uniqueIdField: c.uniqueId,
	})
	if err != nil {
		return err
	}

	// Send request to IDT
	resp, err := http.Post(c.url+endpoint, "application/json", bytes.NewReader(requestBody))
	if err != nil {
		return err
	}

	// Read response
	responseBody, err := ioutil.ReadAll(resp.Body)
	if closeErr := resp.Body.Close(); closeErr != nil {
		return closeErr
	}
	if err != nil {
		return err
	}

	// Return error if non 2xx status code
	if resp.StatusCode < 200 || resp.StatusCode > 299 {
		var errorMessage string

		// Unmarshal response body to retrieve the error message
		// If that doesn't work, use the entire response body instead
		var errResponse internalModels.ErrorMessageResponseWrapper
		if err = json.Unmarshal(responseBody, &errResponse); err == nil {
			errorMessage = errResponse.ErrorMessage
		} else {
			errorMessage = string(responseBody)
		}

		return &Non2xxResponseError{
			errorMessage: enrichErrorMessage(errorMessage, endpoint, resp.StatusCode),
		}
	}

	// 2xx status code: unmarshal response body (errors only if malformed JSON)
	return json.Unmarshal(responseBody, result)
}

func enrichErrorMessage(errorMessage string, endpoint string, statusCode int) string {
	if statusCode == 404 {
		return fmt.Sprintf("the %s request endpoint does not exist - please check that the IDT and SDK versions in use are compatible", endpoint[1:])
	}

	if errorMessage == "" {
		return fmt.Sprintf("error code %d (no more details available)", statusCode)
	}

	return errorMessage
}

func insertKeyValues(bytes []byte, entries map[string]interface{}) ([]byte, error) {
	var unmarshalled map[string]interface{}
	err := json.Unmarshal(bytes, &unmarshalled)
	if err != nil {
		return nil, err
	}

	for k, v := range entries {
		unmarshalled[k] = v
	}

	return json.Marshal(unmarshalled)
}

func fetchEnv(name string, required bool) (string, error) {
	value := os.Getenv(name)

	if value == "" && required {
		return "", fmt.Errorf("%s isn't set in the environment or is empty. Make sure that you're running this test using IDT", name)
	}

	return value, nil
}
