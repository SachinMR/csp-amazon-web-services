// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package idtclient

import (
	"idtclient/models"
)

// Occurs when IDT returns a non 200 response.
type Non2xxResponseError struct {
	errorMessage string
}

func (e *Non2xxResponseError) Error() string {
	return e.errorMessage
}

// Occurs when a command run by the execute on host API exits with a non-zero code.
type ExecuteOnHostCommandFailedError struct {
	Response     *models.ExecuteOnHostResponse
	errorMessage string
}

func (e *ExecuteOnHostCommandFailedError) Error() string {
	return e.errorMessage
}

// Occurs when a command run by the execute on host API times out.
type ExecuteOnHostCommandTimedOutError struct {
	Response     *models.ExecuteOnHostResponse
	errorMessage string
}

func (e *ExecuteOnHostCommandTimedOutError) Error() string {
	return e.errorMessage
}

// Occurs when a command run by the execute on device API exits with a non-zero code.
type ExecuteOnDeviceCommandFailedError struct {
	Response     *models.ExecuteOnDeviceResponse
	errorMessage string
}

func (e *ExecuteOnDeviceCommandFailedError) Error() string {
	return e.errorMessage
}

// Occurs when a command run by the execute on device API times out.
type ExecuteOnDeviceCommandTimedOutError struct {
	Response     *models.ExecuteOnDeviceResponse
	errorMessage string
}

func (e *ExecuteOnDeviceCommandTimedOutError) Error() string {
	return e.errorMessage
}

// Occurs when IDT is unable to resolve a JSON path when getting a value from the context.
type GetContextValueUnresolvedJsonPathsError struct {
	Response     *models.GetContextValueResponse
	errorMessage string
}

func (e GetContextValueUnresolvedJsonPathsError) Error() string {
	return e.errorMessage
}
