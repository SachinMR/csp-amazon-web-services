// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: Apache-2.0

package models

import "idtclient/models"

// Wraps error message responses.
type ErrorMessageResponseWrapper struct {
	ErrorMessage string `json:"errorMessage"`
}

// Wraps send result responses.
type SendResultResponseWrapper struct {
	ErrorMessageResponseWrapper
	Data *models.SendResultResponse `json:"data"`
}

// Wraps copy to device responses.
type CopyToDeviceResponseWrapper struct {
	ErrorMessageResponseWrapper
	Data *models.CopyToDeviceResponse `json:"data"`
}

// Wraps read from device responses.
type ReadFromDeviceResponseWrapper struct {
	ErrorMessageResponseWrapper
	Data *models.ReadFromDeviceResponse `json:"data"`
}

// Wraps poll for notifications responses.
type PollForNotificationsResponseWrapper struct {
	ErrorMessageResponseWrapper
	Data *models.PollForNotificationsResponse `json:"data"`
}

// Wraps get context value responses.
type GetContextValueResponseWrapper struct {
	ErrorMessageResponseWrapper
	Data *models.GetContextValueResponse `json:"data"`
}

// Wraps execute on host responses.
type ExecuteOnHostResponseWrapper struct {
	ErrorMessageResponseWrapper
	Data *models.ExecuteOnHostResponse `json:"data"`
}

// Wraps execute on device responses.
type ExecuteOnDeviceResponseWrapper struct {
	ErrorMessageResponseWrapper
	Data *models.ExecuteOnDeviceResponse `json:"data"`
}
