// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package testPing

import (
	"fmt"
	"go.amzn.com/idt/main/runTestUtils"
	"idtclient"
	"idtclient/models"
	"io/ioutil"
	"regexp"
	"runtime"
	"time"
)

const pingExeFileName = "ping"
const hostToPing = "amazon.com"

func NewPingTest(myClient idtclient.Client) runTestUtils.Test {
	return &pingTest{
		client: myClient,
	}
}

type pingTest struct {
	client idtclient.Client
}

func (t *pingTest) Cleanup() error {
	return nil
}

// Testcase to demo the usage of the ExecuteOnHost operation, using the system's ping utility.
func (t *pingTest) Run() (string, error) {
	// Prepare command-line arguments depending on OS type.
	var cliArgs []string

	switch runtime.GOOS {
	case "windows":
		cliArgs = []string{"-n", "4", hostToPing}
	case "darwin":
		fallthrough
	case "linux":
		cliArgs = []string{"-c", "4", hostToPing}
	default:
		return "", fmt.Errorf("unsupported platform %v", runtime.GOOS)
	}

	// Prepare execute on host request
	myExecuteOnHostRequest := models.ExecuteOnHostRequest{
		Command: models.ExecuteOnHostCommand{
			Timeout: 1 * time.Minute,
			Cmd:     pingExeFileName,
			Args:    cliArgs,
		},
	}

	// Send execute on host request
	myExecuteOnHostResponse, err := t.client.ExecuteOnHost(myExecuteOnHostRequest)
	if err != nil {
		// Check if operation resulted in a timeout or a non-zero exit code, and return a failure rather than an error
		if failedErr, ok := err.(*idtclient.ExecuteOnHostCommandFailedError); ok {
			return fmt.Sprintf("ping exited with error code %v", failedErr.Response.ExitCode), nil
		}

		if _, ok := err.(*idtclient.ExecuteOnHostCommandTimedOutError); ok {
			return "ping timed out - please check your internet connectivity", nil
		}

		return "", err
	}

	// Retrieve entire standard output and locate last ping status
	bytes, err := ioutil.ReadFile(myExecuteOnHostResponse.StdOutFile)
	if err != nil {
		return "", err
	}

	regExp := regexp.MustCompile(`time=([0-9.,]+\s*[a-z]*s)`)
	matches := regExp.FindAllString(string(bytes), -1)

	if len(matches) == 0 {
		return "no ping results - please check your internet connectivity", nil
	}

	fmt.Printf("Observed latency to host %v is %v\n", hostToPing, matches[len(matches)-1][5:])

	return "", nil
}
