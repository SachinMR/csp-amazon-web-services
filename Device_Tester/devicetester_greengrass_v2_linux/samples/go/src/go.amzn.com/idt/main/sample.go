// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package main

import (
	"flag"
	"fmt"
	"go.amzn.com/idt/main/runTestUtils"
	"go.amzn.com/idt/main/testCat"
	"go.amzn.com/idt/main/testLog"
	"go.amzn.com/idt/main/testPing"
	"go.amzn.com/idt/main/testRam"
	"idtclient"
	"idtclient/models"
	"log"
	"os"
	"os/signal"
	"syscall"
)

const (
	testFlag     = "test"
	catTestCase  = "cat"
	logTestCase  = "log"
	ramTestCase  = "ram"
	pingTestCase = "ping"
)

func main() {
	fmt.Println("Running test case")

	/******************************************
		1. Create a client to send request.
	*******************************************/
	myClient, err := idtclient.NewClient()
	if err != nil {
		fmt.Printf("Failed to create client: %s\n", err)
		os.Exit(1)
	}

	/******************************************
		2. Retrieve test to run from test.json.
	*******************************************/
	testPtr := flag.String(testFlag, "", "flag to pass test in")

	// Used by logTest.
	logFilePathPtr := flag.String("logFilePathFromTestArgs", "", "The Log File Path")

	flag.Parse()

	var test runTestUtils.Test

	// According to the test string passed from test.json, we run the corresponding test.
	switch *testPtr {
	case catTestCase:
		test = testCat.NewCatTest(myClient)
	case logTestCase:
		test = testLog.NewLogTest(myClient, logFilePathPtr)
	case ramTestCase:
		test = testRam.NewRamTest(myClient)
	case pingTestCase:
		test = testPing.NewPingTest(myClient)
	default:
		fmt.Printf("Invalid testcase: %s\n", test)
		os.Exit(1)
	}

	/******************************************
		3. Setup signal handler.

		IDT sends an alarm signal to the test executable when it times out. After that, the test case has 5 minutes before it is forcibly killed.
		When the user presses ctrl + c in their shell, the test executable receives an interrupt signal.
		Both these cases indicate that the test should stop running. Before stopping, the test should first clean up the device it's running on.
	*******************************************/
	handleSignals(test)

	/******************************************
		4. Run test.
	*******************************************/
	testResult := RunTestAndCreateResult(test)

	/******************************************
		5. Clean up any resources the test created.

		This cleanup is best effort - if cleanup fails but the test succeeds, the test result sent to IDT is still success.
		In that case information is still printed about the cleanup failure and the executable exits with an error status to let the user know what happened.
	*******************************************/
	cleanupErr := Cleanup(test)

	/******************************************
		6. Call sendResult endpoint to send result to IDT server.
	*******************************************/
	// Create a sendResult request, please navigate to src/idtclient/models/send_result.go to see what SendResultRequest looks like.
	mySendResultRequest := models.SendResultRequest{
		TestResult: testResult,
	}

	// Send sendResult request
	_, err = myClient.SendResult(mySendResultRequest)
	if err != nil {
		fmt.Println("SendResult error: ", err)
		os.Exit(1)
	}

	// If a cleanup error occurred, exit with a non-zero status code to let the user know something went wrong. The logs will have error details.
	if cleanupErr != nil {
		os.Exit(1)
	}

	fmt.Println("Finished running test case")
}

// Cleans up any resources the test created, printing out errors that occur.
func Cleanup(test runTestUtils.Test) error {
	fmt.Println("Cleaning up test resources")

	err := test.Cleanup()

	if err != nil {
		fmt.Println("Error cleaning up test case: ", err)
	}

	return err
}

// Run the test and format the result so it can be sent to IDT through the client
func RunTestAndCreateResult(test runTestUtils.Test) models.TestResult {
	failureInfo, err := test.Run()

	// Create a testResult. please navigate to src/idtclient/models/send_result.go to see comments of TestResult about
	// how to make a well formed test result.
	errLogger := log.New(os.Stderr, "", 0)
	var testResult models.TestResult
	if err != nil {
		testResult = models.TestResult{
			Error: &models.ErrorMessage{
				Type:    "Example Error Type",
				Message: err.Error(),
			},
		}
		errLogger.Println("Test error: ", err.Error())
	} else if failureInfo != "" {
		testResult = models.TestResult{
			Failure: &models.FailureMessage{
				Type:    "Example Failure Type",
				Message: failureInfo,
			},
		}
		errLogger.Println("Test failure: ", failureInfo)
	} else {
		testResult = models.TestResult{
			Passed: true,
		}
	}

	return testResult
}

func handleSignals(test runTestUtils.Test) {
	signals := make(chan os.Signal, 1)

	signal.Notify(signals, syscall.SIGINT, syscall.SIGALRM, syscall.Signal(21)) // 21 is SIGBREAK

	go func() {
		<-signals

		exitCode := 0
		if err := Cleanup(test); err != nil {
			exitCode = 1
		}

		os.Exit(exitCode)
	}()
}
