// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package testLog

import (
	"encoding/json"
	"fmt"
	"go.amzn.com/idt/main/runTestUtils"
	"idtclient"
	"idtclient/models"
	"os"
)

const (
	logEnvVarFromSuite = "LOG_FILE_PATH_FROM_SUITE"
	logEnvVarFromTest  = "LOG_FILE_PATH_FROM_TEST"
)

func NewLogTest(myClient idtclient.Client, logFilePathPtr *string) runTestUtils.Test {
	return &logTest{
		client:         myClient,
		logFilePathPtr: logFilePathPtr,
	}
}

type logTest struct {
	client         idtclient.Client
	logFilePathPtr *string
}

func (l *logTest) Cleanup() error {
	return nil
}

func (l *logTest) Run() (string, error) {
	/*****************************************************************************
	  Method 1. Use GetContextValue to get a value and unmarshall it to a string
	*****************************************************************************/
	// This is a suggested way to get a value from IDT's context.

	// A value returned from GetContextValue will be a json encoded byte array,
	// and can represent many types of json values.

	myGetContextValueRequest := models.GetContextValueRequest{JsonPath: "testData.logFilePath"}
	gcvResp, err := l.client.GetContextValue(myGetContextValueRequest)
	if err != nil {
		return "", err
	}

	// In this case we want a string, so we create a variable to hold it:
	var logFilePath string

	// Unmarshall the response value byte array to our string variable:
	if err = json.Unmarshal(gcvResp.Value, &logFilePath); err != nil {
		return "", err
	}

	fmt.Println("Method 1, the log file path is: " + logFilePath)

	/**********************************************************************************************************
	  Method 2. Use GetContextValue to get an object value and unmarshall it to a struct, then get the string
	**********************************************************************************************************/
	// This is another suggested way to get a value from IDT's context.

	// Get the entire testData object:
	myGetContextValueRequest = models.GetContextValueRequest{JsonPath: "testData"}
	gcvResp, err = l.client.GetContextValue(myGetContextValueRequest)
	if err != nil {
		return "", err
	}

	// Create a struct that tags the keys for the testData values we would like to unmarshall:
	var desiredTestData struct {
		LogFilePath string `json:"logFilePath"`
	}

	if err = json.Unmarshal(gcvResp.Value, &desiredTestData); err != nil {
		return "", err
	}

	fmt.Println("Method 2, the log file path is: " + desiredTestData.LogFilePath)

	/*******************************************************************
	  Method 3. Use GetContextString to get a string value as a string
	*******************************************************************/
	// This is a suggested way to get a string from IDT's context,
	// or use IDT's placeholder replacement on a given string.

	// Unlike GetContextValue, GetContextString returns a string with no unmarshalling needed.
	// This is convenient in our case because we're getting a string, but would not work for other types of json values.
	myGetContextStringRequest := models.GetContextStringRequest{InputString: "{{testData.logFilePath}}"}
	gcsResp, err := l.client.GetContextString(myGetContextStringRequest)
	if err != nil {
		return "", err
	}

	fmt.Println("Method 3, the log file path is: " + gcsResp.Value)

	/*************************************************************************************************
	  Method 4. Use GetContextString to get a string value as a string (+ extra definitions, interpolation)
	*************************************************************************************************/
	// This approach is essentially the same as Method 3, but it also shows how extra definitions work (also applicable to GetContextValue) and how GetContextString's string interpolation works.
	// It prints the same output as Method 3 (besides the method number), but has IDT construct the output string.

	// This is convenient if you'd like to combine multiple strings from the context together without any further string processing.
	// This has the same restrictions as Method 3 (context values must be able to be converted to strings).
	myGetContextStringRequest = models.GetContextStringRequest{
		InputString: "Method {{methodNumber}}, the log file path is: {{testData.logFilePath}}",
		ExtraDefs: map[string]interface{} {
			"methodNumber": 4,
		},
	}
	gcsResp, err = l.client.GetContextString(myGetContextStringRequest)
	if err != nil {
		return "", err
	}

	fmt.Println(gcsResp.Value)

	/*************************************************************************
	  Method 5. Use an environment variable set in suite.json to get a value
	*************************************************************************/
	// This is not a recommended way of getting a value from IDT's context, but here for reference.
	// This method won't work for objects, arrays of anything but strings, or nil values.

	// suite.json defines and environment variable as "{{testData.logFilePath}}",
	// which will be replaced with the proper value when the environment variable is set.

	logFilePathFromSuite := os.Getenv(logEnvVarFromSuite)
	fmt.Println("Method 5, the log file path is: " + logFilePathFromSuite)

	/************************************************************************
	  Method 6. Use an environment variable set in test.json to get a value
	************************************************************************/
	// This is not a recommended way of getting a value from IDT's context, but here for reference.
	// This method won't work for objects, arrays of anything but strings, or nil values.

	// test.json defines an environment variable that holds "{{testData.logFilePath}}",
	// which will be replaced with the proper value when the environment variable is set.

	logFilePathFromTest := os.Getenv(logEnvVarFromTest)
	fmt.Println("Method 6, the log file path is: " + logFilePathFromTest)

	/***************************************************************
	  Method 7. Use a CLI argument set in test.json to get a value
	***************************************************************/
	// This is not a recommended way of getting a value from IDT's context, but here for reference.
	// This method won't work for objects, arrays of anything but strings, or nil values.

	// test.json provides a CLI argument that holds "{{testData.logFilePath}}",
	// which will be replaced with the proper value before being passed as an argument.

	// The logFilePathFromTestArgs argument is captured in sample.go before the call to flag.Parse(),
	// and is passed into this test case as the logFilePathPtr.
	fmt.Println("Method 7, the log file path is: " + *l.logFilePathPtr)

	return "", nil
}
