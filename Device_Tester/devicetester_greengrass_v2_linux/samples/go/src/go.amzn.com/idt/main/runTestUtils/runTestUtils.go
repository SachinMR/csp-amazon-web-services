// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package runTestUtils

import (
	"encoding/json"
	"strings"
)

type Test interface {
	// Run the test.
	Run() (string, error)

	// Cleanup any resources created by the test.
	Cleanup() error
}

// A helper function to print the contents of a struct.
func DeepPrint(data interface{}) string {
	var result []byte
	result, err := json.MarshalIndent(data, "", "")
	resultStr := strings.Replace(string(result), "\n", "", -1)
	if err != nil {
		return err.Error()
	}
	return resultStr
}
