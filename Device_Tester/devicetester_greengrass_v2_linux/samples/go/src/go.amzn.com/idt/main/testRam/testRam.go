// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package testRam

import (
	"fmt"
	"go.amzn.com/idt/main/runTestUtils"
	"idtclient"
	"idtclient/models"
	"strconv"
	"strings"
)

func NewRamTest(myClient idtclient.Client) runTestUtils.Test {
	return &ramTest{
		client: myClient,
	}
}

type ramTest struct {
	client idtclient.Client
}

// No need to do any device cleanup for the ram test because no resources are created.
func (r *ramTest) Cleanup() error {
	return nil
}

func (r *ramTest) Run() (string, error) {
	/******************************************
		Call execute on device operation.
	******************************************/
	// Create a execute on device request, please navigate to src/idtclient/models/execute_on_device.go to see what ExecuteOnDeviceRequest looks like.
	myExecuteOnDeviceRequest := models.ExecuteOnDeviceRequest{
		Command: models.ExecuteOnDeviceCommand{
			CommandLine: "cat /proc/meminfo | grep MemTotal | awk '{print $2}'",
		},
	}
	// Send execute on device request
	myExecuteOnDeviceResponse, err := r.client.ExecuteOnDevice(myExecuteOnDeviceRequest)
	if err != nil {
		return "", err
	}
	fmt.Printf("Execute on device response: %s\n", runTestUtils.DeepPrint(myExecuteOnDeviceResponse))

	/******************************************
		Check memory.
	******************************************/
	totalMemoryStr := string(myExecuteOnDeviceResponse.StdOut)
	totalMemory, err := strconv.Atoi(strings.TrimSpace(totalMemoryStr))
	if err != nil {
		return "", err
	}

	if totalMemory < 512000 {
		return "Device does not meet minimum requirement of 512 Mb RAM", nil
	}

	return "", nil
}
