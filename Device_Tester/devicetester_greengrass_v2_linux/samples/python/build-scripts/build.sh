#!/bin/sh

# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

################################################################################

# variables
PYTHON_INSTALLED=0
PYTHON_VERSION_MIN=3.6.0
PYTHON_VERSION_RECOMMENDED=3.7.0
ALL_DIR_EXIST=1
FILES_COPIED=1
SUITE_NAME=IDTSampleSuitePython
SUITE_VERSION=1.0.1
CURRENT_DIR=$(pwd)
PYTHON_SDK_DIR=${CURRENT_DIR}/../../../sdks/python/idt_client
PYTHON_SUITE_SRC_DIR=${CURRENT_DIR}/../src
PYTHON_SUITE_ROOT_DIR=${CURRENT_DIR}/../configuration/${SUITE_NAME}_${SUITE_VERSION}
TEST_DIR=${CURRENT_DIR}/../../../tests
SUITE_OUTPUT_DIR=${TEST_DIR}/${SUITE_NAME}_${SUITE_VERSION}

################################################################################

# functions
checkRequiredFilesExist() {
    echo "==> Checking if all required directories exist..."

    if [ ! -d ${PYTHON_SDK_DIR} ]; then
        ALL_DIR_EXIST=0
        echo "    ${PYTHON_SDK_DIR} directory not found."
    fi

    if [ ! -d ${PYTHON_SUITE_SRC_DIR} ]; then
        ALL_DIR_EXIST=0
        echo "    ${PYTHON_SUITE_SRC_DIR} directory not found."
    fi

    if [ ! -d ${PYTHON_SUITE_ROOT_DIR} ]; then
        ALL_DIR_EXIST=0
        echo "    ${PYTHON_SUITE_ROOT_DIR} directory not found."
    fi

    if [ ! -d ${TEST_DIR} ]; then
        ALL_DIR_EXIST=0
        echo "    ${TEST_DIR} directory not found."
    fi
}

checkPythonInstalled() {
    echo "==> Checking if Python 3 is installed..."
    if [ -x "$(command -v python3)" ]; then
        echo "    Python 3 is installed!"
        PYTHON_INSTALLED=1
    else
        echo "    WARN: Python 3 could not be found. It must be installed and available as the python3 command. The build will continue, but in order to run the test suite Python (https://python.org/) must be installed. Recommended version >=${PYTHON_VERSION_RECOMMENDED}, Minimum version >=${PYTHON_VERSION_MIN}"
    fi
}

checkPythonVersion() {
    echo "==> Checking Python 3 version..."
    PYTHON_VERSION=$(python3 --version | cut -d ' ' -f 2)
    PYTHON_VERSION_MAJOR=`echo $PYTHON_VERSION | cut -d . -f 1`
    PYTHON_VERSION_MINOR=`echo $PYTHON_VERSION | cut -d . -f 2`
    PYTHON_VERSION_REVISION=`echo $PYTHON_VERSION | cut -d . -f 3`

    PYTHON_VERSION_MIN_MAJOR=`echo $PYTHON_VERSION_MIN | cut -d . -f 1`
    PYTHON_VERSION_MIN_MINOR=`echo $PYTHON_VERSION_MIN | cut -d . -f 2`
    PYTHON_VERSION_MIN_REVISION=`echo $PYTHON_VERSION_MIN | cut -d . -f 3`

    PYTHON_VERSION_RECOMMENDED_MAJOR=`echo $PYTHON_VERSION_RECOMMENDED | cut -d . -f 1`
    PYTHON_VERSION_RECOMMENDED_MINOR=`echo $PYTHON_VERSION_RECOMMENDED | cut -d . -f 2`
    PYTHON_VERSION_RECOMMENDED_REVISION=`echo $PYTHON_VERSION_RECOMMENDED | cut -d . -f 3`

    if [ $PYTHON_VERSION_MAJOR -lt $PYTHON_VERSION_MIN_MAJOR ] ||
        ( [ $PYTHON_VERSION_MAJOR -eq $PYTHON_VERSION_MIN_MAJOR ] &&
          ( [ $PYTHON_VERSION_MINOR -lt $PYTHON_VERSION_MIN_MINOR ] ||
            ( [ $PYTHON_VERSION_MINOR -eq $PYTHON_VERSION_MIN_MINOR ] &&
              [ $PYTHON_VERSION_REVISION -lt $PYTHON_VERSION_MIN_REVISION ] ))); then
        echo "    WARN: At least Python $PYTHON_VERSION_MIN is required, found $PYTHON_VERSION. The build will continue, but you may not be able to run the test suite."
    else
        if [ $PYTHON_VERSION_MAJOR -lt $PYTHON_VERSION_RECOMMENDED_MAJOR ] ||
            ( [ $PYTHON_VERSION_MAJOR -eq $PYTHON_VERSION_RECOMMENDED_MAJOR ] &&
              ( [ $PYTHON_VERSION_MINOR -lt $PYTHON_VERSION_RECOMMENDED_MINOR ] ||
                ( [ $PYTHON_VERSION_MINOR -eq $PYTHON_VERSION_RECOMMENDED_MINOR ] &&
                  [ $PYTHON_VERSION_REVISION -lt $PYTHON_VERSION_RECOMMENDED_REVISION ] ))); then
            echo "    WARN: It's recommended to have at least $PYTHON_VERSION_RECOMMENDED, found $PYTHON_VERSION. The build will continue, but there may be issues."
        else
            echo "    Python version $PYTHON_VERSION is at least the recommended $PYTHON_VERSION_RECOMMENDED!"
        fi
    fi
}

checkPythonLibraries() {
    echo "==> Checking Python 3 libraries..."
    python3 -c "import urllib3"
    if [ $? -ne 0 ]; then
        echo "    WARN: The urllib3 library (https://pypi.org/project/urllib3/) must be installed. The build will continue, but you may not be able to run the test suite."
    else
        echo "    urllib3 is installed!"
    fi
}

createSuite() {
    echo "==> Preparing test suite..."

    cp -r ${PYTHON_SUITE_ROOT_DIR} ${SUITE_OUTPUT_DIR}
    if [ $? -ne 0 ]; then
        FILES_COPIED=0
        return
    fi

    cp -r ${PYTHON_SUITE_SRC_DIR}/suite ${SUITE_OUTPUT_DIR}
    if [ $? -ne 0 ]; then
        FILES_COPIED=0
        return
    fi

    cp -r ${PYTHON_SDK_DIR} ${SUITE_OUTPUT_DIR}/suite/sample_group
    if [ $? -ne 0 ]; then
        FILES_COPIED=0
        return
    fi
}

################################################################################

# prerequisites check
checkRequiredFilesExist

if [ ${ALL_DIR_EXIST} -eq 0 ]; then
    echo "==> ERROR: Some files needed to create the test suite are missing. Please make sure this script is being run from the build scripts directory and that no files have been removed."
    exit 1
fi

checkPythonInstalled

if [ ${PYTHON_INSTALLED} -eq 1 ]; then
    checkPythonVersion
    checkPythonLibraries
fi

# prepare the test suite
createSuite

if [ ${FILES_COPIED} -eq 0 ]; then
    echo "==> ERROR: Failed to copy files. Please check above error message and make sure that you have permission access to make modifications in these two folders and all of it's sub folders."
    exit 1
fi

################################################################################

cat << EOF

################################################################################

==> Now you're ready to run the test suite if your device configuration is already set up!
==> Example commands to start your first test:
        cd ../../../bin/
        # select an IDT command based on your operating system:
        ./devicetester_mac_x86-64 run-suite --suite-id ${SUITE_NAME}_${SUITE_VERSION}
        ./devicetester_linux_x86-64 run-suite --suite-id ${SUITE_NAME}_${SUITE_VERSION}

==> For more information on how to set up device configuration, please see the documentation.
EOF
