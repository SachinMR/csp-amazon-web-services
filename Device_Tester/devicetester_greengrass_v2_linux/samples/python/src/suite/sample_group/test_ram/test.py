# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

from idt_client import ExecuteOnDeviceRequest, ExecuteOnDeviceCommand


class RamTest:
    def __init__(self, client):
        self._client = client

    # Tests that the device has enough RAM:
    # This test uses IDT's execute on device API to retrieve the total memory of the device and compares that to a constant
    def run(self):
        # Send an execute on device request to get the device memory
        command = ExecuteOnDeviceCommand("cat /proc/meminfo | grep MemTotal | awk '{print $2}'")
        execute_on_device_request = ExecuteOnDeviceRequest(command)
        execute_on_device_response = self._client.execute_on_device(execute_on_device_request)

        print("Execute on device response:", execute_on_device_response)

        total_memory = int(execute_on_device_response.stdout)

        if total_memory < 512000:
            return "Device does not meet minimum requirement of 512 Mb RAM"

        return None

    # This test doesn't create any resources that need cleanup, so this method does nothing.
    def cleanup(self):
        pass
