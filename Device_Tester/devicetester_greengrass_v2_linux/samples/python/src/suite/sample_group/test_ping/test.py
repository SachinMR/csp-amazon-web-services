# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

import sys, re

from datetime import timedelta
from idt_client import ExecuteOnHostRequest, ExecuteOnHostCommand, ExecuteOnHostCommandFailedException, ExecuteOnHostCommandTimedOutException

pingExeFileName = 'ping'
hostToPing = 'amazon.com'


class PingTest:
    def __init__(self, client):
        self._client = client

    # Testcase to demo the usage of the ExecuteOnHost operation, using the system's ping utility.
    def run(self):
        # Prepare command-line arguments depending on OS type
        if sys.platform == 'win32':
            cli_args = ["-n", "5", hostToPing]
        elif sys.platform == 'darwin' or sys.platform == 'linux':
            cli_args = ["-c", "5", hostToPing]
        else:
            return "Unsupported platform " + sys.platform

        # Prepare execute on host request
        command = ExecuteOnHostCommand(pingExeFileName, args=cli_args, timeout=timedelta(minutes=1))
        execute_on_host_request = ExecuteOnHostRequest(command)

        # Send execute on host request, catching timeout and a non-zero exit code cases
        try:
            execute_on_host_response = self._client.execute_on_host(execute_on_host_request)
        except ExecuteOnHostCommandTimedOutException as e:
            return "Ping timed out - please check your internet connectivity (%s)" % e
        except ExecuteOnHostCommandFailedException as e:
            return "Ping exited with error code %d (%s)" % (e.response.exit_code, e)

        # Retrieve entire standard output and locate last ping status
        file = open(execute_on_host_response.std_out_file, mode='r')
        stdout_contents = file.read()
        file.close()

        matches = re.compile(r'time=([0-9.,]+\s*[a-z]*s)').findall(stdout_contents)

        if len(matches) == 0:
            return "Could not find a latency in ping's output"

        print("Observed latency to host %s is %s" % (hostToPing, matches[len(matches)-1]))

        return None

    # This test creates files containing ping's stdout and stderr located in the test case
    # artifact directory, which is automatically deleted by IDT after execution.
    def cleanup(self):
        pass
