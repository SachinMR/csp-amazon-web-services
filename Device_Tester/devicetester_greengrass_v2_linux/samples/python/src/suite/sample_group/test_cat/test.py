# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

import os

from idt_client import ExecuteOnDeviceRequest, CopyToDeviceRequest, ExecuteOnDeviceCommand

dut_file_path = 'testFile.txt'
dut_directory_path = '~/testDirectory'
dut_file_in_directory = '~/testDirectory/testFile.txt'

class CatTest:
    def __init__(self, client):
        self._client = client

    # Tests that the cat command on the device functions properly:
    # This test copies a file to the device, uses the cat command on device to get the contents,
    # and then compares that with the local file contents.
    def run(self):
        # ===============================================================
        # Test 1: Copy a file from host agent to device
        # ===============================================================
        # Copy the test file to the device
        local_file_path = os.path.join(os.getcwd(), 'testFile.txt')
        copy_to_device_request = CopyToDeviceRequest(local_file_path, dut_file_path)
        self._client.copy_to_device(copy_to_device_request)

        # Get contents of the copied file
        command = ExecuteOnDeviceCommand(f'cat {dut_file_path}')
        execute_on_device_request = ExecuteOnDeviceRequest(command)
        execute_on_device_response = self._client.execute_on_device(execute_on_device_request)

        print("Execute on device response:", execute_on_device_response)

        # Assert that the file contents are the same
        output = execute_on_device_response.stdout.decode()
        with open(local_file_path) as local:
            if output != local.read():
                return "Local file contents doesn't match remote contents"

        # ===============================================================
        # Test 2: Copy a directory from host agent to device
        # ===============================================================
        # Copy the test directory to the device
        local_directory_path = os.path.join(os.getcwd(), 'testDirectory')
        copy_dir_to_device_request = CopyToDeviceRequest(local_directory_path, dut_directory_path)
        self._client.copy_to_device(copy_dir_to_device_request)

        # Get contents of the copied file under the copied directory
        command_for_dir = ExecuteOnDeviceCommand(f'cat {dut_file_in_directory}')
        execute_on_device_for_dir_request = ExecuteOnDeviceRequest(command_for_dir)
        execute_on_device_for_dir_response = self._client.execute_on_device(execute_on_device_for_dir_request)

        print("Execute on device response:", execute_on_device_for_dir_response)

        # Assert that the file contents are the same
        output_for_dir = execute_on_device_for_dir_response.stdout.decode()
        local_file_in_dir_path = os.path.join(local_directory_path, 'testFile.txt')
        with open(local_file_in_dir_path) as local:
            if output_for_dir != local.read():
                return "Local file contents doesn't match remote contents"
        return None

    # This test copies a file to the device. In order to clean up that created resource, the file needs to be deleted.
    def cleanup(self):
        # ==================================================================
        # Cleanup 1: Cleanup the file copied from host agent to device
        # ==================================================================
        command_to_cleanup_file = ExecuteOnDeviceCommand(f'rm {dut_file_path}')
        execute_on_device_to_cleanup_file_request = ExecuteOnDeviceRequest(command_to_cleanup_file)
        self._client.execute_on_device(execute_on_device_to_cleanup_file_request)

        # ==================================================================
        # Cleanup 2: Cleanup the directory copied from host agent to device
        # ==================================================================
        command_to_cleanup_dir = ExecuteOnDeviceCommand(f'rm {dut_file_in_directory} && rmdir {dut_directory_path}')
        execute_on_device_to_cleanup_dir_request = ExecuteOnDeviceRequest(command_to_cleanup_dir)
        self._client.execute_on_device(execute_on_device_to_cleanup_dir_request)
