#!/bin/sh

# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: MIT-0

################################################################################

# variables
JAVA_INSTALLED=0
MAVEN_INSTALLED=0
ALL_DIR_EXIST=1
FILES_COPIED=1
JAR_FILE_GENERATED=1
JAVA_VERSION_MIN=8
SUITE_NAME=IDTSampleSuiteJava
SUITE_VERSION=1.0.1
CURRENT_DIR=$(pwd)
JAVA_PKG_DIR=${CURRENT_DIR}/..
JAVA_SDK_ROOT_DIR=${CURRENT_DIR}/../../../sdks/java
JAVA_SDK_DIR=${JAVA_SDK_ROOT_DIR}/idtclient
JAVA_SUITE_SRC_DIR=${CURRENT_DIR}/../src
JAVA_SUITE_ROOT_DIR=${CURRENT_DIR}/../configuration/${SUITE_NAME}_${SUITE_VERSION}
TEST_DIR=${CURRENT_DIR}/../../../tests
SUITE_OUTPUT_DIR=${TEST_DIR}/${SUITE_NAME}_${SUITE_VERSION}

################################################################################

# functions
checkRequiredFilesExist() {
  echo "==> Checking if all required directories exist..."

  if [ ! -d ${JAVA_SDK_DIR} ]; then
      ALL_DIR_EXIST=0
      echo "    ${JAVA_SDK_DIR} directory not found."
  fi

  if [ ! -d ${JAVA_SUITE_ROOT_DIR} ]; then
      ALL_DIR_EXIST=0
      echo "    ${JAVA_SUITE_ROOT_DIR} directory not found."
  fi

  if [ ! -d ${JAVA_PKG_DIR}/src ]; then
      ALL_DIR_EXIST=0
      echo "    ${JAVA_PKG_DIR}/src directory not found."
  fi

  if [ ! -d ${TEST_DIR} ]; then
      ALL_DIR_EXIST=0
      echo "    ${TEST_DIR} directory not found."
  fi
}

checkJavaInstalled() {
  echo "==> Checking if Java is installed..."
  if [ -x "$(command -v java)" ]; then
      echo "    Java is installed!"
      JAVA_INSTALLED=1
  fi
}

checkJavaVersion() {
  echo "==> Checking Java version..."
  JAVA_VERSION=$(java -version 2>&1 | awk -F '"' '/version/ {print $2}')
  JAVA_VERSION_MAJOR=`echo $JAVA_VERSION | cut -d . -f 2`

  if [ ${JAVA_VERSION_MAJOR} -lt ${JAVA_VERSION_MIN} ]; then
    echo "    WARN: It's recommended to have at least Java $JAVA_VERSION_MIN, found $JAVA_VERSION_MAJOR.The build will continue, but there may be issues."
  else
    echo "    Java version $JAVA_VERSION_MAJOR is at least $JAVA_VERSION_MIN!"
  fi
}

checkBuildDependencies() {
  echo "==> Checking build dependencies..."
  if [ -x "$(command -v mvn)" ]; then
      echo "    Maven is installed!"
      MAVEN_INSTALLED=1
  fi
}

setUpFiles() {
  echo "==> Preparing files before compiling..."

  # copy test suite to tests folder
  cp -r ${CURRENT_DIR}/../configuration/${SUITE_NAME}_${SUITE_VERSION} ${CURRENT_DIR}/../../../tests
  if [ $? -ne 0 ]; then
      FILES_COPIED=0
      return
  fi
}

compileJavaFiles() {
  echo "==> Compiling sample test suite..."
  JAVA_SDK_JAR=${JAVA_SDK_ROOT_DIR}/target/Bridgekeeper-ClientSDK-Java-1.0.jar
  Java_SUITE_JAR=${JAVA_PKG_DIR}/target/JavaAmzn-IDTSampleTestSuite-1.0.jar
  JAVA_BUILD_DESTINATION=${CURRENT_DIR}/../../../tests/${SUITE_NAME}_${SUITE_VERSION}/suite/sample_group

  # build SDK package to generate Bridgekeeper-ClientSDK-Java-1.0.jar
  cd ${JAVA_SDK_ROOT_DIR} && mvn install
  if [ ! -f ${JAVA_SDK_JAR} ]; then
      echo "    ${JAVA_SDK_JAR} not found, build failed."
      JAR_FILE_GENERATED=0
      return
  fi

  # build sample suite package to generate JavaAmzn-IDTSampleTestSuite-1.0.jar
  cd ${JAVA_PKG_DIR} && mvn install
  if [ ! -f ${Java_SUITE_JAR} ]; then
      echo "    ${Java_SUITE_JAR} not found, build failed."
      JAR_FILE_GENERATED=0
      return
  fi

  cp ${Java_SUITE_JAR} ${CURRENT_DIR}/../../../tests/${SUITE_NAME}_${SUITE_VERSION}
}

cleanup() {
  echo "==> Cleaning up temporary files..."
  rm -r ${JAVA_SDK_ROOT_DIR}/target
  rm -r ${JAVA_PKG_DIR}/target
}
################################################################################

# prerequisites check
checkRequiredFilesExist

if [ ${ALL_DIR_EXIST} -eq 0 ]; then
    echo "==> ERROR: Some files needed to create the test suite are missing. Please make sure this script is being run from the build scripts directory and that no files have been removed."
    exit 1
fi

checkJavaInstalled
if [ ${JAVA_INSTALLED} -eq 0 ]; then
    echo "==> ERROR: Java is not installed. Please install Java (>=8) before running the build script."
    exit 1
else
  checkJavaVersion
fi

checkBuildDependencies
if [ ${MAVEN_INSTALLED} -eq 0 ]; then
  echo "==> ERROR: Maven is not installed. Please install Maven before running the build script."
  exit 1
fi

# prepare the test suite
setUpFiles
if [ ${FILES_COPIED} -eq 0 ]; then
    echo "==> ERROR: Failed to copy files. Please check above error message and make sure that you have permission access to make modifications in the folder and all of it's sub folders."
    exit 1
fi

compileJavaFiles
if [ ${JAR_FILE_GENERATED} -eq 0 ]; then
    echo "==> ERROR: Failed to build packages. Please check above error message and rerun the script after error fixed."
    exit 1
fi

# clean up
cleanup

################################################################################

cat << EOF

################################################################################

==> Now you're ready to run the test suite if your device configuration is already set up!
==> Example commands to start your first test:
        cd ../../../bin/
        # select an IDT command based on your operating system:
        ./devicetester_mac_x86-64 run-suite --suite-id ${SUITE_NAME}_${SUITE_VERSION}
        ./devicetester_linux_x86-64 run-suite --suite-id ${SUITE_NAME}_${SUITE_VERSION}

==> For more information on how to set up device configuration, please see the documentation.
EOF