// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package com.aws.samplesuite.testPing;

import com.amazonaws.iot.idt.IDTClient;
import com.amazonaws.iot.idt.model.ExecuteOnHostRequest;
import com.amazonaws.iot.idt.model.ExecuteOnHostResponse;
import com.amazonaws.iot.idt.model.ExecuteOnHostCommand;
import com.aws.samplesuite.test.AbstractTest;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * TestPing.
 */
public class TestPing extends AbstractTest {
    /**
     * TestPing.
     * @param idt - IDT Client.
     */
    public TestPing(IDTClient idt) { super(idt); }

    private static final String pingExeFileName = "ping";
    private static final String hostToPing = "amazon.com";

    /**
     * run method.
     * Testcase to demo the usage of the ExecuteOnHost operation, using the system's ping utility.
     */
    @Override
    public String run() throws Exception {
        System.out.println("Running testPing test case");

        // Prepare command-line arguments depending on OS type.
        String OSName = System.getProperty("os.name");
        List<String> cliArgs = new ArrayList<>();

        if(OSName.startsWith("Windows")) {
            cliArgs.add("-n");

        } else if(OSName.startsWith("Mac") || OSName.startsWith("Linux")) {
            cliArgs.add("-c");
        } else {
            return String.format("Unsupported platform %s", OSName);
        }

        cliArgs.add("6");
        cliArgs.add(hostToPing);

        ExecuteOnHostResponse myExecuteOnHostResponse = idt.executeOnHost(ExecuteOnHostRequest.builder()
                .command(ExecuteOnHostCommand.of(pingExeFileName, cliArgs))
                .build());
        if(myExecuteOnHostResponse.exitCode() != 0) {
            String errorMessage;
            if(myExecuteOnHostResponse.timedOut()) {
                errorMessage = "ping timed out - please check your internet connectivity";
            } else {
                errorMessage= String.format("Ping exited with error code %d", myExecuteOnHostResponse.exitCode());
            }
            return errorMessage;
        }

        // Retrieve entire standard output and locate last ping status
        String filePath = myExecuteOnHostResponse.stdOutFile();

        String fileContent = "";
        try {
            fileContent = String.join(System.lineSeparator(), Files.readAllLines(Paths.get(filePath)));
        } catch (IOException e) {
            throw new Exception(String.format("read file content failed with error %s", e.getMessage()));
        }

        Pattern pattern = Pattern.compile("time=([0-9.,]+\\s*[a-z]*s)");
        Matcher matcher = pattern.matcher(fileContent);
        if(!matcher.find()) {
            return "Could not find a latency in ping's output";
        }
        System.out.println(String.format("Observed latency to host %s is %s", hostToPing, matcher.group(1)));
        return "";
    }

    /**
     * cleanup method.
     * This test creates files containing ping's stdout and stderr located in the test case
     * artifact directory, which is automatically deleted by IDT after execution.
     */
    @Override
    public String cleanup() {
        return "";
    }
}
