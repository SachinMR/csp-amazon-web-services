// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package com.aws.samplesuite.testCat;

import com.amazonaws.iot.idt.IDTClient;
import com.amazonaws.iot.idt.exception.IDTServiceException;
import com.amazonaws.iot.idt.model.*;
import com.aws.samplesuite.test.AbstractTest;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
/**
 * TestCat.
 */
public class TestCat extends AbstractTest {
    /**
     * TestCat.
     * @param idt - IDT Client.
     */
    public TestCat(IDTClient idt) {
        super(idt);
    }

    private static final String fileName = "testFile.txt";
    private static final String dirName = "testDirectory";
    private static final String destFilePath = "~/testFile.txt";
    private static final String destDirPath = "~/testDirectory";
    private static final String destFileInDir = "~/testDirectory/testFile.txt";
    /**
     * run method.
     */
    @Override
    public String run() throws Exception {
        System.out.println("Running testCat test case");

        /*****************************************************************************
         Test 1. Copy a file from host agent to device
         *****************************************************************************/

        /******************************************
         Call copy to device operation.
         ******************************************/

        String localFilePath = Paths.get(System.getProperty("user.dir"), fileName).toString();
        try {
            CopyToDeviceResponse myCopyToDeviceResponse = idt.copyToDevice(CopyToDeviceRequest.builder()
                    .sourcePath(localFilePath)
                    .targetPath(destFilePath)
                    .build());
            System.out.println(String.format("Execute on device response: %s", myCopyToDeviceResponse.toString()));
        }
        catch (IDTServiceException e) {
            return String.format("Copy file to device failed, from: %s, to: %s with error message: %s", localFilePath, destFilePath, e.getMessage());
        }
        System.out.println(String.format("Copy file to device succeed, from: %s, to: %s", localFilePath, destFilePath));

        /******************************************
         Call execute on device operation.
         ******************************************/
        ExecuteOnDeviceCommand command = ExecuteOnDeviceCommand.builder()
                .commandLine("cat " + destFilePath)
                .build();

        // Create a execute on device request
        ExecuteOnDeviceResponse myExecuteOnDeviceResponse = idt.executeOnDevice(ExecuteOnDeviceRequest.builder()
                .command(command)
                .build());
        System.out.println(String.format("Execute 'cat' command on device response: %s", new String(myExecuteOnDeviceResponse.stdout(), StandardCharsets.UTF_8)));

        if (myExecuteOnDeviceResponse.exitCode() != 0) {
            return String.format("Execute 'cat' command on device failed with error: %s", new String(myExecuteOnDeviceResponse.stderr(), StandardCharsets.UTF_8));
        }

        /******************************************
         Compare files to ensure that the file contents are the same.
         ******************************************/
        String catOutPut = new String(myExecuteOnDeviceResponse.stdout(), StandardCharsets.UTF_8);
        String originalFileSt = "";
        try {
            originalFileSt = String.join(System.lineSeparator(), Files.readAllLines(Paths.get(localFilePath)));
        } catch (IOException e) {
            throw new Exception(String.format("Failed to read %s file with error: %s", localFilePath, e.getMessage()));
        }

        if (!originalFileSt.equals(catOutPut)) {
            return String.format("Local file contents: %s doesn't match remote contents: %s ", originalFileSt, catOutPut);
        }
        System.out.println(String.format("Local file contents: %s matches remote contents: %s ", originalFileSt, catOutPut));

        /*****************************************************************************
         Test 2. Copy a directory from host agent to device
         *****************************************************************************/

        /******************************************
         Call copy to device operation.
         ******************************************/

        String localDirPath = Paths.get(System.getProperty("user.dir"), dirName).toString();

        try {
            CopyToDeviceResponse myCopyToDeviceResponse = idt.copyToDevice(CopyToDeviceRequest.builder()
                    .sourcePath(localDirPath)
                    .targetPath(destDirPath)
                    .build());
            System.out.println(String.format("Copy directory to device response: %s", myCopyToDeviceResponse.toString()));
        }
        catch (IDTServiceException e) {
            return String.format("Copy directory to device failed, from: %s, to: %s with error: %s", localDirPath, destDirPath, e.getMessage());
        }
        System.out.println(String.format("Copy directory to device succeed, from: %s, to: %s", localDirPath, destDirPath));
        /******************************************
         Call execute on device operation.
         ******************************************/
        command = ExecuteOnDeviceCommand.builder()
                .commandLine("cat " + destFileInDir)
                .build();

        // Send execute on device request
        ExecuteOnDeviceResponse myDirExecuteOnDeviceResponse = idt.executeOnDevice(ExecuteOnDeviceRequest.builder()
                .command(command)
                .build());
        System.out.println(String.format("Execute on device response: %s", new String(myExecuteOnDeviceResponse.stdout(), StandardCharsets.UTF_8)));

        if (myDirExecuteOnDeviceResponse.exitCode() != 0) {
            return String.format("Execute on device failed with error: %s", new String(myExecuteOnDeviceResponse.stderr(), StandardCharsets.UTF_8));
        }

        /******************************************
         Compare files to ensure that the file contents are the same.
         ******************************************/
        String localFilePathInDir = Paths.get(localDirPath, fileName).toString();

        String catOutputInDir = new String(myDirExecuteOnDeviceResponse.stdout(), StandardCharsets.UTF_8);
        String originalFileStInDir = "";
        try {
            originalFileStInDir = String.join(System.lineSeparator(), Files.readAllLines(Paths.get(localFilePathInDir)));
        } catch (IOException e) {
            throw new Exception(String.format("Failed to read %s file with error: %s", localFilePathInDir, e.getMessage()));
        }

        if (!originalFileStInDir.equals(catOutputInDir)) {
            return String.format("Local file contents: %s doesn't match remote contents: %s ", originalFileStInDir, catOutputInDir);
        }
        System.out.println(String.format("Local file contents: %s matches remote contents: %s ", originalFileStInDir, catOutputInDir));

        return "";
    }

    /**
     * cleanup method.
     * The cat test creates a file on the device. In order to cleanup the test, it needs to be deleted.
     */
    @Override
    public String cleanup() {
        /*****************************************************************************
         Cleanup 1. Cleanup the file copied from host agent to device
         *****************************************************************************/

        ExecuteOnDeviceCommand myExecuteOnDeviceCommand = ExecuteOnDeviceCommand.builder()
                .commandLine(String.format("rm %s", destFilePath))
                .build();

        ExecuteOnDeviceResponse myExecuteOnDeviceResponse = idt.executeOnDevice(ExecuteOnDeviceRequest.builder()
                .command(myExecuteOnDeviceCommand)
                .build());

        String errorMessage = "";
        if (myExecuteOnDeviceResponse.exitCode() != 0) {
            errorMessage = new String(myExecuteOnDeviceResponse.stderr(), StandardCharsets.UTF_8);
        }
        System.out.println("Execute on device to remove test file");


        /*****************************************************************************
         Cleanup 2. Cleanup the directory copied from host agent to device
         *****************************************************************************/

        ExecuteOnDeviceCommand myDirExecuteOnDeviceCommand = ExecuteOnDeviceCommand.builder()
                .commandLine(String.format("rm %s && rmdir %s ", destFileInDir, destDirPath))
                .build();

        ExecuteOnDeviceResponse myDirExecuteOnDeviceResponse = idt.executeOnDevice(ExecuteOnDeviceRequest.builder()
                .command(myDirExecuteOnDeviceCommand)
                .build());

        if (myDirExecuteOnDeviceResponse.exitCode() != 0) {
            String errorMessage2 = new String(myDirExecuteOnDeviceResponse.stderr(), StandardCharsets.UTF_8);
            errorMessage = errorMessage == ""? errorMessage2 : String.format("%s; and %s",errorMessage, errorMessage2);
        }
        if(errorMessage != "") {
            return String.format("Couldn't clean up the file copied to device with error: %s", errorMessage);
        }
        System.out.println("Execute on device to remove test directory");
        return "";
    }
}
