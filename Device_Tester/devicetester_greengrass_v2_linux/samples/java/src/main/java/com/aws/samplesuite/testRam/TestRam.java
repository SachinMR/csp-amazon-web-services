
// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package com.aws.samplesuite.testRam;

import com.amazonaws.iot.idt.IDTClient;
import com.amazonaws.iot.idt.model.ExecuteOnDeviceCommand;
import com.amazonaws.iot.idt.model.ExecuteOnDeviceRequest;
import com.amazonaws.iot.idt.model.ExecuteOnDeviceResponse;
import com.aws.samplesuite.test.AbstractTest;

import java.nio.charset.StandardCharsets;


/**
 * TestRam.
 */
public class TestRam extends AbstractTest {
    /**
     * TestRam.
     * @param idt - IDT Client.
     */
    public TestRam(IDTClient idt) {
        super(idt);
    }

    /**
     * run method.
     * Tests that the device has enough RAM:
     * This test uses IDT's execute on device API to retrieve the total memory of the device and compares that to a constant
     */
    @Override
    public String run() throws Exception {
        System.out.println("Running testRam test case");

        /******************************************
         Call execute on device operation.
         ******************************************/

        // Create a device command
        ExecuteOnDeviceCommand command = ExecuteOnDeviceCommand.builder()
                .commandLine("cat /proc/meminfo | grep MemTotal | awk '{print $2}'")
                .build();

        // Send execute on device request
        ExecuteOnDeviceResponse myExecuteOnDeviceResponse = idt.executeOnDevice(ExecuteOnDeviceRequest.builder()
                .command(command)
                .build());

        if(myExecuteOnDeviceResponse.exitCode() != 0) {
            return String.format("Couldn't retrieve the ram on the device with error: %s", new String(myExecuteOnDeviceResponse.stderr(), StandardCharsets.UTF_8));
        }
        String totalMemoryStr = new String(myExecuteOnDeviceResponse.stdout(), StandardCharsets.UTF_8);
        System.out.println(String.format("Execute on device response: %s", totalMemoryStr));

        /******************************************
         Check memory.
         ******************************************/

        int totalMemory;
        try {
            totalMemory = Integer.parseInt(totalMemoryStr.replaceAll("\\s", ""));
        }
        catch (NumberFormatException e) {
            throw new Exception(String.format("parse RAM value failed with error %s", e.getMessage()));
        }

        if(totalMemory < 512000) {
            return "Device does not meet minimum requirement of 512 Mb RAM";
        }

        return "";
    }

    /**
     * cleanup method.
     * No need to do any device cleanup for the ram test because no resources are created.
     */
    @Override
    public String cleanup() {
        return "";
    }
}
