// Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
// SPDX-License-Identifier: MIT-0

package com.aws.samplesuite.testLog;

import com.amazonaws.iot.idt.IDTClient;
import com.amazonaws.iot.idt.exception.IDTServiceException;
import com.amazonaws.iot.idt.model.GetContextStringRequest;
import com.amazonaws.iot.idt.model.GetContextStringResponse;
import com.amazonaws.iot.idt.model.GetContextValueRequest;
import com.amazonaws.iot.idt.model.GetContextValueResponse;
import com.aws.samplesuite.test.AbstractTest;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * TestLog.
 */
public class TestLog extends AbstractTest {
    /**
     * TestLog.
     * @param idt - IDT Client.
     * @param testArgs - log file path.
     */
    public TestLog(IDTClient idt, String testArgs) {

        super(idt);
        logFilePathFromTestArgs = testArgs.substring(testArgs.indexOf("=")+1);
    }
    private String logFilePathFromTestArgs;
    private static final String logEnvVarFromSuite = "LOG_FILE_PATH_FROM_SUITE";
    private static final String logEnvVarFromTest  = "LOG_FILE_PATH_FROM_TEST";

    /**
     * run method.
     */
    @Override
    public String run() throws Exception {
        /*****************************************************************************
         Method 1. Use GetContextValue to get a value and encode it to a string
         *****************************************************************************/
        // This is a suggested way to get a value from IDT's context.

        // A value returned from GetContextValue will be a json encoded byte array,
        // and can represent many types of json values.
        GetContextValueResponse myGetContextValueResponse;
        try {
            myGetContextValueResponse = idt.getContextValue(GetContextValueRequest.builder()
                    .jsonPath("testData.logFilePath")
                    .build());
        }
        catch (IDTServiceException e) {
            return String.format("Failed to copy to get context value with error: %s", e.getMessage());
        }

        // In this case we are expecting a string to be resolved.
        String logFilePath = new String(myGetContextValueResponse.value(), StandardCharsets.UTF_8);
        System.out.println(String.format("Method 1, the log file path is: %s",  logFilePath));

        /**********************************************************************************************************
         Method 2. Use GetContextValue to get an object value and encode it to a struct, then get the string
         **********************************************************************************************************/
        // This is another suggested way to get a value from IDT's context.

        // Get the entire testData object:
        try {
            myGetContextValueResponse = idt.getContextValue(GetContextValueRequest.builder()
                    .jsonPath("testData")
                    .build());
        }
        catch (IDTServiceException e) {
            return String.format("Failed to copy to get context value with error: %s", e.getMessage());
        }

        // Read the returned object and extract the expected field:
        try {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readValue(new String(myGetContextValueResponse.value(), StandardCharsets.UTF_8), JsonNode.class);
            logFilePath = jsonNode.get("logFilePath").asText();
        } catch (IOException e) {
            throw new Exception(String.format("Failed to extract context value with error: %s", e.getMessage()));
        }

        System.out.println(String.format("Method 2, the log file path is: %s", logFilePath));

        /*******************************************************************
         Method 3. Use GetContextString to get a string value as a string
         *******************************************************************/
        // This is a suggested way to get a string from IDT's context,
        // or use IDT's placeholder replacement on a given string.

        // Unlike GetContextValue, GetContextString returns a string with no unmarshalling needed.
        // This is convenient in our case because we're getting a string, but would not work for other types of json values.

        GetContextStringResponse myGetContextStringResponse;
        try{
            myGetContextStringResponse = idt.getContextString(GetContextStringRequest.builder()
                    .inputString("{{testData.logFilePath}}")
                    .build());
        }
        catch (IDTServiceException e) {
            return String.format("Failed to get context string with error: %s", e.getMessage());
        }
        System.out.println(String.format("Method 3, the log file path is: %s", myGetContextStringResponse.value()));

        /*************************************************************************************************
         Method 4. Use GetContextString to get a string value as a string (+ extra definitions, interpolation)
         *************************************************************************************************/
        // This approach is essentially the same as Method 3, but it also shows how extra definitions work (also applicable to GetContextValue) and how GetContextString's string interpolation works.
        // It prints the same output as Method 3 (besides the method number), but has IDT construct the output string.

        // This is convenient if you'd like to combine multiple strings from the context together without any further string processing.
        // This has the same restrictions as Method 3 (context values must be able to be converted to strings).

        try{
            myGetContextStringResponse = idt.getContextString(GetContextStringRequest.builder()
                    .inputString("Method 4, the log file path is: {{testData.logFilePath}}")
                    .build());
        }
        catch (IDTServiceException e) {
            return String.format("Failed to get context string with error: %s", e.getMessage());
        }

        System.out.println(myGetContextStringResponse.value());

        /*************************************************************************
         Method 5. Use an environment variable set in suite.json to get a value
         *************************************************************************/
        // This is not a recommended way of getting a value from IDT's context, but here for reference.
        // This method won't work for objects, arrays of anything but strings, or nil values.

        // suite.json defines and environment variable as "{{testData.logFilePath}}",
        // which will be replaced with the proper value when the environment variable is set.

        logFilePath = System.getenv(logEnvVarFromSuite);
        System.out.println(String.format("Method 5, the log file path is: %s", logFilePath));

        /************************************************************************
         Method 6. Use an environment variable set in test.json to get a value
         ************************************************************************/
        // This is not a recommended way of getting a value from IDT's context, but here for reference.
        // This method won't work for objects, arrays of anything but strings, or nil values.

        // test.json defines an environment variable that holds "{{testData.logFilePath}}",
        // which will be replaced with the proper value when the environment variable is set.
        logFilePath = System.getenv(logEnvVarFromTest);
        System.out.println(String.format("Method 6, the log file path is: %s", logFilePath));

        /***************************************************************
         Method 7. Use a CLI argument set in test.json to get a value
         ***************************************************************/
        // This is not a recommended way of getting a value from IDT's context, but here for reference.
        // This method won't work for objects, arrays of anything but strings, or nil values.

        // test.json provides a CLI argument that holds "{{testData.logFilePath}}",
        // which will be replaced with the proper value before being passed as an argument.

        // The logFilePathFromTestArgs argument is captured in sample.go before the call to flag.Parse(),
        // and is passed into this test case as the logFilePathPtr.

        System.out.println(String.format("Method 7, the log file path is: %s", logFilePathFromTestArgs));
        return "";
    }

    /**
     * cleanup method.
     */
    @Override
    public String cleanup() {
        return "";
    }
}
