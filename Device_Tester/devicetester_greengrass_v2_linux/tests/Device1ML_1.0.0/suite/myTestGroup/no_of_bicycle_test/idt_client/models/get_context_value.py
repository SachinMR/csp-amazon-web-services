# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from .utils import Model, BytesConverter


class GetContextValueRequest(Model):
    """
    A get context value request.

    :ivar str json_path: The JSON Path for the value to get from the context.
        When getting anything but the root object, the leading "$." is optional.
    :ivar extra_defs: Optional extra definitions to insert into the IDT context for just this request.
        These definitions are merged at the root of the context object.
        For example, if {"foo" : "bar"} is provided here, then "foo" is now a valid JSON path to query in the IDT context even though IDT does not provide that path. It will be resolved to "bar".
        /nNote:
        - Using keys IDT already provides (e.g. testData) should be avoided. If there is a key collision, values will resolve to the extra definition value and not IDT's value.
        - Keys must not start with "_IDT_" - these are reserved for internal use.
    :vartype extra_defs: dict[string, any]
    """
    _attributes = ['json_path', 'extra_defs']

    def __init__(self, json_path, extra_defs=None):
        self.json_path = json_path
        self.extra_defs = extra_defs


class GetContextValueResponse(Model):
    """
    The response to a get context value request.

    :ivar bytes value: A JSON encoded byte slice which is the result of getting the value from the context.
        This can be deserialized into the concrete type which it represents, using e.g. standard json.loads().
        If an error occurred, this field should not be used.
        This is the main result of the request.
    :ivar encountered_json_paths: JSON paths encountered while getting the value. This includes paths in any placeholders as well as the query itself.
        If an error occurred, may contain useful information or may be None depending on the error.
        This is optional metadata to be used if needed.
        E.g. This can be used to validate the presence of certain placeholders.
    :vartype encountered_json_paths: list[string]
    :ivar unresolved_json_paths: A subset of EncounteredJsonPaths which contains paths whose values could not be resolved.
        Possible reasons for resolution failure include circular references or attempting
        to coerce types to a string that can't be coerced.
        If an error occurred, may contain useful information or may be None depending on the error.
        This is optional metadata to be used if needed.
        E.g. This can be used to construct a different error message than the one IDT provides.
    :vartype unresolved_json_paths: list[string]
    """
    _attributes = [('value', BytesConverter), 'encountered_json_paths', 'unresolved_json_paths']

    def __init__(self, value, encountered_json_paths, unresolved_json_paths):
        self.value = value
        self.encountered_json_paths = encountered_json_paths
        self.unresolved_json_paths = unresolved_json_paths
