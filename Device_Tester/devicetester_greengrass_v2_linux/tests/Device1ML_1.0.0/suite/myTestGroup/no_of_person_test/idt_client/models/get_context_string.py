# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from .utils import Model


class GetContextStringRequest(Model):
    """
    A get context string request.

    :ivar str input_string: The string to do placeholder evaluation on.
        Placeholder evaluation:
        - Any placeholders of the form {{jsonPath}} within the string will be queried from the context and replaced with their actual values.
          For example, if the context was {"foo": "bar"}, then "a {{foo}} c" would result in "a bar c".
        - A string without placeholders is returned unmodified.
        When getting anything but the root object, the leading "$." for JSON paths within placeholders is optional.
    :ivar extra_defs: Optional extra definitions to insert into the IDT context for just this request.
        These definitions are merged at the root of the context object.
        For example, if {"foo" : "bar"} is provided here, then "foo" is now a valid JSON path to query in the IDT context even though IDT does not provide that path. It will be resolved to "bar".
        /nNote:
        - Using keys IDT already provides (e.g. testData) should be avoided. If there is a key collision, values will resolve to the extra definition value and not IDT's value.
        - Keys must not start with "_IDT_" - these are reserved for internal use.
    :vartype extra_defs: dict[string, any]
    """
    _attributes = ['input_string', 'extra_defs']

    def __init__(self, input_string, extra_defs=None):
        self.input_string = input_string
        self.extra_defs = extra_defs


class GetContextStringResponse(Model):
    """
    The response to a get context string request.

    :ivar str value: A string which has had placeholder evaluation performed on it.
        If an error occurred, this field should not be used.
        This is the main result of the request.
    :ivar encountered_json_paths: JSON paths encountered while getting the value. This includes paths in any placeholders as well as the query itself.
        If an error occurred, may contain useful information or may be None depending on the error.
        This is optional metadata to be used if needed.
        E.g. This can be used to validate the presence of certain placeholders.
    :vartype encountered_json_paths: list[string]
    :ivar unresolved_json_paths: A subset of EncounteredJsonPaths which contains paths whose values could not be resolved.
        Possible reasons for resolution failure include circular references or attempting
        to coerce types to a string that can't be coerced.
        If an error occurred, may contain useful information or may be None depending on the error.
        This is optional metadata to be used if needed.
        E.g. This can be used to construct a different error message than the one IDT provides.
    :vartype unresolved_json_paths: list[string]
    """
    _attributes = ['value', 'encountered_json_paths', 'unresolved_json_paths']

    def __init__(self, value, encountered_json_paths, unresolved_json_paths):
        self.value = value
        self.encountered_json_paths = encountered_json_paths
        self.unresolved_json_paths = unresolved_json_paths
