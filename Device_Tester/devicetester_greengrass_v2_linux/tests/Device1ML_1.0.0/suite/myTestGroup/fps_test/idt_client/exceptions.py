# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

class ClientException(Exception):
    """
    The base class for all exceptions explicitly thrown by the client.
    """
    def __init__(self, info, details=None):
        message = info + (f" Details: {details}" if details is not None else "")
        super().__init__(message)
        self.message = message


class DeserializationException(ClientException):
    """
    Thrown when a server response couldn't be deserialized into a model.
    """
    def __init__(self, details):
        super().__init__(f'Failed to deserialize the server response. Please make sure you have the right version of the Python SDK for IDT.', details)


class Non2xxResponseException(ClientException):
    """
    Thrown when a server response isn't 2xx.
    """
    def __init__(self, error_message):
        super().__init__(error_message)


class DomainException(ClientException):
    """
    Subclasses of this are thrown when the server completes an API request successfully, but the response is not considered successful by that API.
    """
    def __init__(self, response, error_message):
        super().__init__(error_message)
        self.response = response


class ExecuteOnHostCommandFailedException(DomainException):
    """
    Thrown when a command executed by the execute on host API exits with a non-zero exit code.
    """
    def __init__(self, response, error_message):
        super().__init__(response, error_message)


class ExecuteOnHostCommandTimedOutException(DomainException):
    """
    Thrown when a command executed by the execute on host API times out.
    """
    def __init__(self, response, error_message):
        super().__init__(response, error_message)


class ExecuteOnDeviceCommandFailedException(DomainException):
    """
    Thrown when a command executed by the execute on device API exits with a non-zero exit code.
    """
    def __init__(self, response, error_message):
        super().__init__(response, error_message)


class ExecuteOnDeviceCommandTimedOutException(DomainException):
    """
    Thrown when a command executed by the execute on device API exits times out.
    """
    def __init__(self, response, error_message):
        super().__init__(response, error_message)


class GetContextValueUnresolvedJsonPathsException(DomainException):
    """
    Thrown when IDT is unable to resolve a JSON path when getting a value from the context.
    May indicate the presence of unresolved JSON path(s), or cyclic dependency between placeholders.
    """
    def __init__(self, response, error_message):
        super().__init__(response, error_message)
