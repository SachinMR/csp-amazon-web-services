# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from base64 import b64encode, b64decode
from datetime import timedelta
import json

from ... import DeserializationException

DAYS_TO_SECONDS = 24 * 60 * 60
SECONDS_TO_MICROSECONDS = 10**6
MICROSECONDS_TO_NANOSECONDS = 10**3
SECONDS_TO_NANOSECONDS = 10**6


class ModelConverter:
    """
    Converts models to/from dictionaries, handling attributes appropriately.
    """
    def __init__(self, model_class):
        self._model_class = model_class

    @classmethod
    def serialize(cls, value, for_idt):
        """
        Converts the model into a dictionary, serializing all of its attributes.

        :param for_idt: Whether serialization is happening for IDT to consume.
        :param value: The value to serialize.
        """
        result = {}

        for attribute_name, converter in map(cls._unpack_attribute, value._attributes):
            attribute_value = getattr(value, attribute_name)

            if converter is None or attribute_value is None:
                serialized = attribute_value
            else:
                serialized = converter.serialize(attribute_value, for_idt)

            # Include None when serializing for printing but not when making requests
            if attribute_value is not None or not for_idt:
                result_key = attribute_name if not for_idt else cls._serialize_name(attribute_name)
                result[result_key] = serialized

        return result

    def deserialize(self, value):
        """
        Converts the dictionary, or json encoded dictionary, into an instance of the provided model class.

        :param value: The value to deserialize.
        """
        try:
            dictionary = value if isinstance(value, dict) else json.loads(value)

            args = {}
            for attribute_name, converter in map(self._unpack_attribute, self._model_class._attributes):
                attribute_value = dictionary.get(self._serialize_name(attribute_name))
                deserialized = attribute_value if converter is None else converter.deserialize(attribute_value)
                args[attribute_name] = deserialized

            return self._model_class(**args)
        except DeserializationException as e:
            raise e
        except Exception as e:
            raise DeserializationException(str(e))

    @staticmethod
    def _unpack_attribute(value):
        """
        Unpacks info of a model class attribute into a two-tuple consisting of (Python attribute name, associated converter).
        If the value is a string, the second component is set to None. Otherwise the value should be a two-tuple.

        :param value: The value to unpack.
        :return: The unpacked tuple.
        """
        return (value, None) if isinstance(value, str) else value

    @staticmethod
    def _serialize_name(name):
        """
        Converts snake case to lower camel case. Assumes that:
         - the input string is not empty
         - the input string has no punctuation except for underscores

        :param name: The name to serialize.
        :return: The serialized name
        """
        upper_camel_case = name.title().replace('_', '')
        return name[0] + upper_camel_case[1:]


class BytesConverter:
    """
    For IDT:
        Converts bytes to and from base64 encoded strings.
    For humans:
        Converts bytes to non-base64 encoded strings that are readable.
    """
    @staticmethod
    def serialize(value, for_idt):
        """
        If the value is bytes, serialize it.
        Otherwise, returns the value as is.

        :param value: The value to serialize.
        :param for_idt: Whether serialization is happening for IDT to consume.
        """
        if isinstance(value, bytes):
            if for_idt:
                return b64encode(value).decode()
            else:
                return value.decode()
        else:
            return value

    @staticmethod
    def deserialize(value):
        """
        Tries to convert a base 64 encoded string to bytes. Raises an exception if that fails.

        :param value: The value to deserialize.
        """
        if value is None:
            return None
        return b64decode(value)


class TimeDeltaConverter:
    """
    For IDT:
        Converts timedeltas to ints in nanoseconds.
    For humans:
        Converts timedeltas to readable time durations, e.g. h::mm::ss.
    """
    @staticmethod
    def serialize(value, for_idt):
        """
        If the value is a timedelta, serialize it.
        Otherwise, returns the value as is.

        :param value: The value to serialize.
        :param for_idt: Whether serialization is happening for IDT to consume.
        """
        if isinstance(value, timedelta):
            if for_idt:
                return (value.microseconds + (value.seconds + value.days * DAYS_TO_SECONDS) * SECONDS_TO_MICROSECONDS) * MICROSECONDS_TO_NANOSECONDS
            else:
                return str(value)
        return value

    @staticmethod
    def deserialize(value):
        """
        Tries to convert an int in nanoseconds to a timedelta. Raises an exception if that fails.

        :param value: The value to deserialize.
        """
        if value is None:
            return None
        return timedelta(seconds=(value / SECONDS_TO_NANOSECONDS))
