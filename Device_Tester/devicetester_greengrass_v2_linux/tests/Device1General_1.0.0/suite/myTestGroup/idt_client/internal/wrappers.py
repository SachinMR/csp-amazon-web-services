# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from ..models.utils import Model, ModelConverter
from ..models import SendResultResponse, CopyToDeviceResponse, ReadFromDeviceResponse, PollForNotificationsResponse, \
    GetContextValueResponse, ExecuteOnHostResponse, ExecuteOnDeviceResponse


class ErrorMessageResponseWrapper(Model):
    """
    Wraps error message responses.
    """
    _attributes = ['error_message']

    def __init__(self, error_message=None):
        self.error_message = error_message


class SendResultResponseWrapper(Model):
    """
    Wraps send result responses.
    """
    _attributes = [('data', ModelConverter(SendResultResponse)), 'error_message']

    def __init__(self, data=None, error_message=None):
        self.data = data
        self.error_message = error_message


class CopyToDeviceResponseWrapper(Model):
    """
    Wraps copy to device responses.
    """
    _attributes = [('data', ModelConverter(CopyToDeviceResponse)), 'error_message']

    def __init__(self, data=None, error_message=None):
        self.data = data
        self.error_message = error_message


class ReadFromDeviceResponseWrapper(Model):
    """
    Wraps read from device responses.
    """
    _attributes = [('data', ModelConverter(ReadFromDeviceResponse)), 'error_message']

    def __init__(self, data=None, error_message=None):
        self.data = data
        self.error_message = error_message


class PollForNotificationsResponseWrapper(Model):
    """
    Wraps poll for notifications responses.
    """
    _attributes = [('data', ModelConverter(PollForNotificationsResponse)), 'error_message']

    def __init__(self, data=None, error_message=None):
        self.data = data
        self.error_message = error_message


class GetContextValueResponseWrapper(Model):
    """
    Wraps get context value responses.
    """
    _attributes = [('data', ModelConverter(GetContextValueResponse)), 'error_message']

    def __init__(self, data=None, error_message=None):
        self.data = data
        self.error_message = error_message


class ExecuteOnHostResponseWrapper(Model):
    """
    Wraps execute on host responses.
    """
    _attributes = [('data', ModelConverter(ExecuteOnHostResponse)), 'error_message']

    def __init__(self, data=None, error_message=None):
        self.data = data
        self.error_message = error_message


class ExecuteOnDeviceResponseWrapper(Model):
    """
    Wraps execute on device responses.
    """
    _attributes = [('data', ModelConverter(ExecuteOnDeviceResponse)), 'error_message']

    def __init__(self, data=None, error_message=None):
        self.data = data
        self.error_message = error_message
