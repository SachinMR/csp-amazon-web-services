# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from .utils import Model, ModelConverter


class FailureMessage(Model):
    """
    Info about a test case failure. This should be used when the test logic runs, but assertions/checks fail.

    :ivar str type: The type of the failure
    :ivar str message: A message explaining why the test failed.
    """
    _attributes = ['type', 'message']

    def __init__(self, type, message):
        self.type = type
        self.message = message


class ErrorMessage(Model):
    """
    Info about a test case error. This should be used when something prevents the test logic from running.

    :ivar str type: The type of the failure.
    :ivar str message: A message explaining why the test errored.
    """
    _attributes = ['type', 'message']

    def __init__(self, type, message):
        self.type = type
        self.message = message


class SkippedMessage(Model):
    """
    Info about why a test case was skipped. This should be used if the test case decided to skip itself.

    :ivar str message: A message explaining why the test case was skipped.
    """
    _attributes = ['message']

    def __init__(self, message):
        self.message = message


class TestResult(Model):
    """
    The result of a test.

    In order to send a well formed request, exactly one of the following must be true:
     - failure is not None
     - error is not None
     - skipped is not None
     - passed is True

    :ivar str test_category_name: The category name of the test case that ran.
                                  Will map to JUnit's "classname" attribute in the final test report.
                                  If left None or blank, will default to the concatenation of the suite ID and group ID of the test case.
    :ivar str test_case_name: The name of the test case that ran.
                              If left None or blank, will default to test case ID.
    :ivar failure: Info about the test case failure. Leave None if the test case didn't fail.
    :vartype failure: :class:`.FailureMessage`
    :ivar error: Info about the test case error. Leave None if the test case didn't error.
    :vartype error: :class:`.ErrorMessage`
    :ivar skipped: Info describing why the test case was skipped. Leave None if the test wasn't skipped.
                   Leave None if the test case wasn't skipped.
    :vartype skipped: :class:`.SkippedMessage`
    :ivar bool passed: Indication that the test was successfully executed.
    :ivar int duration: Execution time of the test case in seconds.
                        If set to None, test case execution time will be omitted in the generated report.
                        If set to 0, test case execution time will show in the report and be set to the elapsed time since the test case executable was launched.
                        Otherwise, test case execution time will show in the report as this value.
                        Note: regardless of the above, test group execution time will show in the generated report as the elapsed time all test case executables in the group took to run.
    """
    _attributes = ['test_category_name', 'test_case_name',
                   ('failure', ModelConverter(FailureMessage)), ('error', ModelConverter(ErrorMessage)),
                   ('skipped', ModelConverter(SkippedMessage)), 'passed', 'duration']

    def __init__(self, test_category_name=None, test_case_name=None, failure=None, error=None, skipped=None, passed=False, duration=None):
        self.passed = passed
        self.test_category_name = test_category_name
        self.test_case_name = test_case_name
        self.failure = failure
        self.error = error
        self.skipped = skipped
        self.duration = duration


class SendResultRequest(Model):
    """
    A send result request.

    :ivar test_result: The result of the test case.
    :vartype test_result: :class:`.TestResult`
    """
    _attributes = [('test_result', ModelConverter(TestResult))]

    def __init__(self, test_result):
        self.test_result = test_result


class SendResultResponse(Model):
    """
    The response to a send result request.

    IDT does not include any information in the response.
    A call without an exception indicates the result was received successfully.
    """
    _attributes = []
