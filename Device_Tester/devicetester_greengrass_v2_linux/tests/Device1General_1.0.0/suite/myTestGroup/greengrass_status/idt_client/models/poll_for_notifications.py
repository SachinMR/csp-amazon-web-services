# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from .utils import Model


class PollForNotificationsResponse(Model):
    """
    The response to a poll for notifications request.

    :ivar bool cancellation_requested: This field is set to true when IDT requests test cases to stop running, cleanup, and exit.
                                       The value will be true for the duration of the cancellation request - i.e. repeated polling will continue to retrieve true even after the first call.
                                       Test cases should take care to only run any necessary cleanup actions once.
    """
    _attributes = ['cancellation_requested']

    def __init__(self, cancellation_requested):
        self.cancellation_requested = cancellation_requested
