# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

import json

from .converters import ModelConverter


class Model:
    """
    The base class for request/response models. Can be serialized/deserialized.
    """
    _attributes = []

    def __repr__(self):
        """
        Returns a string representation of the model.
        """
        return self._to_str()

    def __eq__(self, other):
        """
        Checks for model equality.

        :param other: The other object to compare against.
        :return: Whether both objects are equal.
        """
        if not isinstance(other, type(self)):
            return False

        return self._to_dict() == other._to_dict()

    def __str__(self):
        """
        Returns a string representation of the model.

        """
        return self._to_str()

    def _to_dict(self):
        """
        Returns this model as a dictionary.
        """
        return ModelConverter(type(self)).serialize(self, False)

    def _to_str(self):
        """
        Returns this model as a string.
        """
        return json.dumps(self._to_dict())
