# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

from .utils import Model


class CopyToDeviceRequest(Model):
    """
    A copy to device request.

    :ivar str source_path: The absolute source path to the file or directory to copy.
    /nNote:
	  - If this is a file, then the target path must also be a file.
	  - If this is a directory, then the target path must also be a directory.
    :ivar str target_path: The absolute target path to where the file or directory should be copied.
	/nNote:
    - When copying files: if the target file already exists it will be overwritten.
    - When copying directories: the target directory must not already exist.
    - If IDT is running on Windows and if the target path contains '/', all '\' will be replaced with '/' to ensure cross platform copy succeeds.
    :ivar bool use_sudo: Whether to use the sudo command to do the copy. Sudo must be present on the remote device if this is true.
    :ivar str device_name: The name of the device to copy to. Leave None to use the device under test - otherwise use the name specified in the test.json for resource devices.
    """
    _attributes = ['source_path', 'target_path', 'use_sudo', 'device_name']

    def __init__(self, source_path, target_path, use_sudo=False, device_name=None):
        self.source_path = source_path
        self.target_path = target_path
        self.use_sudo = use_sudo
        self.device_name = device_name


class CopyToDeviceResponse(Model):
    """
    The response to a copy to device request.

    IDT does not include any information in the response.
    A call without an exception indicates the file or directory was copied successfully.
    """
    _attributes = []
