# This file contains *system properties* that will be applied to Log4j at initialization.
# This can only be used for configuration settings that appear in this list:
# https://logging.apache.org/log4j/2.x/manual/configuration.html#SystemProperties

# Properties applied here will be used as the default for *all* services at Amazon that rely on Log4j, so we should be deliberate about adding new ones.
# Service teams can override these choices by specifying their own values as normal JVM System Properties, generally set in wrapper scripts.

# Async logging in Log4j2 uses a ring buffer with a set of background threads flushing the log messages to disk. If the ring buffer fills up, the default
# behavior of Log4j 2.10+ is to block the threads attempting to log until space in the ring buffer is freed up. This is an availability risk that has resulted
# in a number of LSEs over the years. Instead, we configure Log4j to discard messages below ERROR when the ring buffer is full, ensuring the service can
# continue operating as normal.
# Note that the ring buffer can fill up either if the application is logging too much, or if the flushing process slows or is halted. If the flushing process
# is not making *any* progress, such as with a full disk, then the application threads will still block if they attempt to log at ERROR or FATAL levels. We make
# the assumption that if a service is logging ERROR or FATAL messages, the request is likely to fail anyway, and the cost/benefit tradeoff for logging errors
# tips in favor of blocking.
log4j2.asyncQueueFullPolicy=Discard
log4j2.discardThreshold=ERROR
