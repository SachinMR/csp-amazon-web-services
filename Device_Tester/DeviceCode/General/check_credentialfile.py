from pathlib import Path

path_to_file = '/home/root/.aws/credentials'
path = Path(path_to_file)

if path.is_file():
    print(f'The file exists')
else:
    print(f'The file does not exist')
