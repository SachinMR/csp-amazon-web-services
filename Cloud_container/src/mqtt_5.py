import threading
import json
import time
from awscrt import io, mqtt5, auth, http 
from src.mqtt5_builder import MQTT5_builder,on_publish_received,on_lifecycle_connection_success,on_lifecycle_connection_failure,on_lifecycle_stopped,exponential_backoff,backoff_jitter,jitter_backoff
from concurrent.futures import Future
from awsiot import mqtt_connection_builder, mqtt5_client_builder

received_count = 0                                                                                                                                
received_all_event = threading.Event()
future_stopped = Future()
backoff = Future()
future_connection_success = Future()  
TIMEOUT = 17500

class MQTT_5:                                                       
    def __init__(self):
        self.mqtt5_client = MQTT5_builder("Basic Connect - Make a MQTT connection.")
        self.load_cloud_config()
        # self.adv_object = advisor()
        self.endpoint = None
        self.cert = self.advisor_config["certificatePath"]
        self.privatekey = self.advisor_config["privateKeyPath"]
        self.rootCA = self.advisor_config["rootCAPath"]

    def load_cloud_config(self):                                             
        config_file = open("config/advisor_credential.json")        
        self.advisor_config = json.load(config_file)
    
    def connect_to_cloud(self):
        clientId = "device_csp"
        port = 8883
        self.client = self.mqtt5_client.build_mqtt5_client(endpoint=self.endpoint,
        on_publish_received=on_publish_received,
        on_lifecycle_stopped=on_lifecycle_stopped,
        on_lifecycle_connection_success=on_lifecycle_connection_success,
        on_lifecycle_connection_failure=on_lifecycle_connection_failure,
        backoff_jitter=backoff_jitter,
        jitter_backoff= jitter_backoff,
        exponential_backoff=exponential_backoff)
        print("MQTT5 Client Created")
        self.client.start()
        lifecycle_connect_success_data = future_connection_success.result(TIMEOUT)
        sleep = exponential_backoff(exp_backoff)
        wait = jitter_backoff(jitter)
        connack_packet = lifecycle_connect_success_data.connack_packet
        negotiated_settings = lifecycle_connect_success_data.negotiated_settings

    def mqtt_subscribe(self,topic):
        print("Subscribing to topic '{}'...".format(message_topic))
        subscribe_future = client.subscribe(subscribe_packet=mqtt5.SubscribePacket(
        subscriptions=[mqtt5.Subscription(
            topic_filter=message_topic,
            qos=mqtt5.QoS.AT_LEAST_ONCE)]))
        suback = subscribe_future.result(TIMEOUT)
        print("Subscribed with {}".format(suback.reason_codes))                                                                                               
    
    def mqtt_publish(self, topic, message):
        print("Publishing message to topic '{}': {}".format(topic, message))
        publish_future = client.publish(mqtt5.PublishPacket(
                topic=message_topic,
                payload=message_string,
                qos=mqtt5.QoS.AT_LEAST_ONCE
            ))
        publish_completion_data = publish_future.result(TIMEOUT)
        print("PubAck received with {}".format(repr(publish_completion_data.puback.reason_code)))
        time.sleep(1)




 
        
