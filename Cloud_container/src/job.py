import threading
import json
import time
from awscrt import io, mqtt, auth, http 
from src.mqtt_builder import MQTT_builder, on_connection_interrupted, on_connection_resumed, on_resubscribe_complete


received_count = 0                                                                                                                                
received_all_event = threading.Event()  

class JOB:                                                       
    def __init__(self):
        self.mqtt_builder = MQTT_builder("Basic Connect - Make a MQTT connection.")
        self.load_cloud_config()
        self.endpoint = None
        self.rootCA = self.advisor_config["aws_iot_cert_folder"] + "/AmazonRootCA1.pem"
        self.cert = self.advisor_config["aws_iot_cert_folder"] + "/" + "certificate.pem.crt"
        self.privatekey = self.advisor_config["aws_iot_cert_folder"] + "/" + "private.pem.key"

    def load_cloud_config(self):                                             
        config_file = open("config/cloud_credential.json")        
        self.advisor_config = json.load(config_file)
    
    def connect_to_cloud(self):
        clientId = "dev_adv"
        port = 8883
        self.conn_handler = self.mqtt_builder.build_mqtt_connection(on_connection_interrupted, 
                                                                on_connection_resumed, 
                                                                self.endpoint, port, self.cert, 
                                                                self.privatekey, self.rootCA, clientId)

        print("Connecting to endpoint with client ID") 
        print("connecting........")                                                       
        try:
            connect_future = self.conn_handler.connect()  
            connect_future.result()
            print("Connected to AWS IOT as MQTT client!")
        except:
            time.sleep(10)
            try:    
                connect_future = self.conn_handler.connect()  
                connect_future.result()
            except:
                time.sleep(10)
            

    def reboot_callback(self, topic, payload, dup, qos, retain, **kwargs):                                                   
        print(type(payload.decode("utf-8")))                                                                                                                                                         
        command = str(payload.decode("utf-8")).strip("\n{}][").replace(" ", "").split(":")                                                                                                           
        print("received command:", command)                                                                                                       
 
    def job_subscribe(self, qos=mqtt.QoS.AT_LEAST_ONCE):
        topic = "$aws/things/cloud_thing/jobs/notify-next"
        subscribe_future, packet_id = self.conn_handler.subscribe(topic=topic, qos=qos, callback=self.reboot_callback)            
        subscribe_result = subscribe_future.result()                                                                                                  
     
    def job_publish(self,qos=mqtt.QoS.AT_LEAST_ONCE): 
        topic = "armcsp/pub"
        message = "payload"                                                    
        print("Publishing message to topic '{}': {}".format(topic, message))                                                                                                             
        message_json = json.dumps(message)                                                                     
        self.conn_handler.publish(topic=topic, payload=message_json, qos=qos, retain=True)     
