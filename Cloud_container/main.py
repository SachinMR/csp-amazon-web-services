import json
import argparse
import time
from flask import Flask, request, redirect, flash, jsonify
from src.argconfig import ArgConf
from src.cloud_agent import AwsAgent
# /
import subprocess
flag = 0
# /

ap = argparse.ArgumentParser()
args = ArgConf(ap)

app = Flask(__name__)

cloud_agent = AwsAgent(args["cloudwatch_post_interval"],args["advisor"])

@app.route('/video', methods=['POST'])
def text():
    # /
    global flag
    # /
    if request.method == 'POST':
        # /
        print(flag)
        if flag == 10:
            kvs_config_file = open("config/kvs_credential.json")
            kvs_new_config = json.load(kvs_config_file)  
            #cmd = "gst-launch-1.0 rtspsrc location=rtsp://127.0.0.1:8555/cam ! rtph264depay ! h264parse ! video/x-h264,stream-format=avc,alignment=au ! kvssink stream-name="+kvs_new_config["streamName"]+" storage-size=1024 framerate=5 access-key="+kvs_new_config["accessKey"]+" secret-key="+kvs_new_config["secretKey"]+" aws-region="+kvs_new_config["region"]
            #cmd = "gst-launch-1.0 rtspsrc location=rtsp://127.0.0.1:8555/cam ! rtph264depay ! h264parse ! kvssink stream-name="+kvs_new_config["streamName"]+" framerate=5 max-latency=3000 fragment-duration=20000 access-key="+kvs_new_config["accessKey"]+" secret-key="+kvs_new_config["secretKey"]+" aws-region="+kvs_new_config["region"]
            cmd = "gst-launch-1.0 rtspsrc location=rtsp://127.0.0.1:8555/cam ! rtph264depay ! h264parse ! kvssink stream-name="+kvs_new_config["streamName"]+" storage-size=1024 access-key="+kvs_new_config["accessKey"]+" secret-key="+kvs_new_config["secretKey"]+" aws-region="+kvs_new_config["region"]
            subprocess.Popen(cmd, shell=True)
        flag += 1
        # /
        cloud_agent.send_metrics_to_cloud(request.json['cloud_metrics'])
        end_rep = time.time()
        return jsonify({"message" : "Sent data to cloud", "End_to_End_pipeline_time": end_rep})

if __name__ == '__main__': 
    app.run(host="0.0.0.0",port=args['cloud_port'])

